<?php
session_start();
$message = '';
$message1 = '';
if (isset($_SESSION['message'])) {
    $message = $_SESSION['message'];
    unset($_SESSION['message']);
}
if (isset($_SESSION['message1'])) {
    $message1 = $_SESSION['message1'];
    unset($_SESSION['message1']);
}
if (!isset($_COOKIE['parent_key'])) {
    $_SESSION['message'] = 'ログインしてください。';
    header('Location: login.php');
    exit;
}


$dsn = 'mysql:host=localhost;dbname=hew2022_it42107;charset=utf8mb4';
$db_user = 'hew2022_it42107';
$db_pass = '';

try {
    $pdo = new PDO($dsn, $db_user, $db_pass);
    $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $pdo->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);


    $sql = "SELECT * FROM parentbalance WHERE parent_id = :parent_id";
    $qry = $pdo->prepare($sql);
    $qry->bindValue(':parent_id', $_COOKIE['parent_key']);
    $qry->execute();
    $parentbalance = $qry->fetch();

    $sql = "SELECT * FROM children WHERE parent_id = :parent_id";
    $qry = $pdo->prepare($sql);
    $qry->bindValue(':parent_id', $_COOKIE['parent_key']);
    $qry->execute();

    $sql = "SELECT * FROM childbalance WHERE child_id = :child_id";
    $qry2 = $pdo->prepare($sql);
    foreach ($qry->fetchAll() as $q) {
        $qry2->bindValue(':child_id', $q['child_id']);
        $qry2->execute();
        $child = $qry2->fetch();
        $child_id[] = $q['child_id'];
        $child_name[] = $q['child_name'];
        $child_balance[] = $child['child_balance'];
    }
} catch (PDOException $e) {
    echo 'DB接続エラー ： ' . $e->getMessage();
}



?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>17.電子マネー管理、残高表示ページ</title>
    <link rel="stylesheet" href="css/reset.css">
    <link rel="stylesheet" href="css/style.css">
    <meta name="robots" content="none,noindex,nofollow">
</head>

<body>
    <header class="header">
        <a href="index.php">
            <img src="images/logo001.png" alt="Cent Disco" class="header_logo">
        </a>
        <nav class="gnav">
            <ul class="menu">
                <li><a href="shop.php">Shop</a></li>
                <li><a href="login.php">MyPage&Login</a></li>
                <li><a href="contact.php">Contact</a></li>
                <li>
                    <a href="cart.php">
                        <img src="images/cart.png" alt="cart" class="header_cart">
                    </a>
                </li>
            </ul>
        </nav>
    </header>

    <div class="main-content">

        <h2 class="body__title">電子マネーの管理</h2>

        <dl class="form-content">
            <dt class="form-content__title" style="height: 60px">あなたの残高</dt>
            <dd class="form-content__input" style="height: 60px">
                <?php
                echo "<input value='" . $parentbalance['parent_balance'] . "円' disabled>"
                ?>
            </dd>

            <output style="color:red"><?php echo $message;?></output>
            <form action="money_charge.php" style="margin-bottom: 80px" class="form-content__form" method="post">
                <dt class="form-content__subtitle account_info">電子マネーの追加</dt>
                <dd class="form-content__input account_info">
                    <input type="number" name="money_parent" value="5000" oninput="javascript:if(this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);" maxlength="6">
                    <br><small style="font-size: 12px;">登録されている情報をもとに電子マネーを購入します</small>
                </dd>
                <dd class="form-content__submit"><input type="submit" value="電子マネーを追加" onclick="return confirm('電子マネーを購入します。よろしいですか？')"></dd>
            </form>
        </dl>

        <hr>
        <dl class="form-content">
        <output style="color:red"><?php echo $message1;?></output>
            <?php
            for ($i = 0; $i < count($child_id); $i++) {
                echo '<dt class="form-content__subtitle" style="height: 100px;">お子様の残高</dt>';
                echo '<dd class="form-content__input" style="height: 60px;">';
                echo "<p style='font-size:16px;margin-bottom:10px;'>" . $child_name[$i] . '様 - ID:' . $child_id[$i] . "</p>";
                echo "<input value='" . $child_balance[$i] . "円' disabled style='margin-bottom: 40px'>";
                echo '</dd>';

                echo '<form action="money_charge.php"  style="margin-bottom: 80px" class="form-content__form" method="post">';
                echo '<dt class="form-content__subtitle account_info">電子マネーの追加</dt>';
                echo '<dd class="form-content__input account_info">';
                echo '<input type="number" name="money_child" value="5000" oninput="javascript:if(this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);"maxlength="6">';
                echo '<input type="hidden" name="child_id" value="'. $child_id[$i] . '">';
                echo '</dd>';
                echo '<dd class="form-content__submit">';
                echo '<input type="submit" value="電子マネーを送る" onclick="return confirm(\'電子マネーを送ります。よろしいですか？\')">';
                echo '</dd>';
                echo '</form> ';
            }
            ?>
            
            <form action="mypage_parent.php" style="margin-top: 50px;">
                <dd class="form-content__submit"><input type="submit" value="マイページへ"></dd>
            </form>
        </dl>


    </div>
    <footer class="footer">
        <p>&copy;Cent Disco</p>
    </footer>
</body>

</html>