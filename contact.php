<?php
session_start();
$message = '';
if (isset($_SESSION['message'])) {
    $message = $_SESSION['message'];
    unset($_SESSION['message']);
}
$mail = '';
$text = '';
if(isset($_COOKIE['mail'],$_COOKIE['text'])){
    $mail = $_COOKIE['mail'];
    $text = $_COOKIE['text'];
    setcookie('mail', '', time()-6000);
    setcookie('text', '', time()-6000);
}
?>
<!DOCTYPE html>
<html lang="ja">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>お問い合わせ</title>
    <link rel="stylesheet" href="css/reset.css">
    <link rel="stylesheet"href="css/style.css">
    <meta name="robots" content="none,noindex,nofollow">
</head>

<body>
<header class="header">
    <a href="index.php">
        <img src="images/logo001.png" alt="Cent Disco" class="header_logo">
    </a>
    <nav class="gnav">
        <ul class="menu">
            <li><a href="shop.php">Shop</a></li>
            <li><a href="login.php">MyPage&Login</a></li>
            <li><a href="contact.php">Contact</a></li>
            
            <li>
              <a href="cart.php">
                <img src="images/cart.png" alt="cart" class="header_cart">
            </a>
            </li>
        </ul>
    </nav>
</header>

<main class="main-content">
    <h2 class="body__title">Contact-お問い合わせ</h2>

    <dl class="form-content">
        <output style="color:red; margin-bottom:30px;"><?php echo $message;?></output>
        <form class="form-content" method="post" action="contact_confirm.php">
            <dt class="form-content__subtitle">01 メールアドレス</dt>
            <dd class="form-content__input"><input type="email" name="mail" value=<?php echo $mail ?>></dd>
            <dt class="form-content__subtitle">02 メールアドレス(確認)</dt>
            <dd class="form-content__input"><input type="email" name="mail_confirm"></dd>
            <dt class="form-content__subtitle">03 お問い合わせの種類</dt>
            <dd class="form-content__select">
                <select name="genre" id="genre">
                    <option value="電子マネーについて">電子マネーについて</option>
                    <option value="アカウントについて">アカウントについて</option>
                    <option value="ミッション機能について">ミッション機能について</option>
                    <option value="その他">その他</option>
                </select>
            </dd>
            <dt class="form-content__subtitle">04 お問い合わせ内容</dt>
            <dd class="form-content__textarea">
                <textarea name="text" id="text"><?php echo $text; ?></textarea>
            </dd>
            <dd class="form-content__submit"><input type="submit" value="確認画面へ"></dd>
        </form>
    </dl>
</main>

<footer class="footer">
    <p>&copy;Cent Disco</p>
</footer>
    
</body>
</html>