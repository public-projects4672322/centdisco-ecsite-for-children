<?php
$dsn = 'mysql:host=localhost;dbname=hew2022_it42107;charset=utf8mb4';
$db_user = 'hew2022_it42107';
$db_pass = '';

try{
  $pdo = new PDO($dsn, $db_user, $db_pass);
  $pdo->setAttribute(PDO::ATTR_ERRMODE,PDO::ERRMODE_EXCEPTION);
  $pdo->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);

  $sql = "INSERT INTO badge VALUES(:badge_id, :badge_path)";
  $qry = $pdo->prepare($sql);

  for($i = 1; $i <= 63; $i++){
    if($i < 10){
      $id = 'badge0' . $i;
      $path = 'icon0' . $i . '.png';
    }else{
      $id = 'badge' . $i;
      $path = 'icon' . $i . '.png';
    }
    $qry->bindValue(':badge_id', $id);
    $qry->bindValue(':badge_path', $path);
    $qry->execute();
  }



} catch (PDOException $e) {
  echo 'DB接続エラー ： ' . $e->getMessage();
}