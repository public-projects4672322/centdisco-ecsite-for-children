<?php
session_start();
if (!isset($_COOKIE['child_key'])) {
  $_SESSION['message'] = 'ログインしてください。';
	header('Location: login.php');
  exit;
}
if(!isset($_POST['badge_change'])){
  $_SESSION['message'] = '下記の内容を選択してください。';
  header('Location: child_change.php');
  exit;
}


if($_POST['badge'] == ""){
  $_SESSION['message'] = '下記の内容を選択してください。';
  header('Location: child_change.php');
}


$dsn = 'mysql:host=localhost;dbname=hew2022_it42107;charset=utf8mb4';
$db_user = 'hew2022_it42107';
$db_pass = '';

try{
  $pdo = new PDO($dsn, $db_user, $db_pass);
  $pdo->setAttribute(PDO::ATTR_ERRMODE,PDO::ERRMODE_EXCEPTION);
  $pdo->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);

  $sql = "UPDATE level SET badge_id_1 = :badge_id_1, badge_id_2 = :badge_id_2, badge_id_3 = :badge_id_3 WHERE child_id = :child_id";
  $qry = $pdo->prepare($sql);
  if(isset($_POST['badge'][0])){
    $qry->bindValue(':badge_id_1', $_POST['badge'][0]);
  }else{
    $qry->bindValue(':badge_id_1', NULL);
  }
  if(isset($_POST['badge'][1])){
    $qry->bindValue(':badge_id_2', $_POST['badge'][1]);
  }else{
    $qry->bindValue(':badge_id_2', NULL);
  }
  if(isset($_POST['badge'][2])){
    $qry->bindValue(':badge_id_3', $_POST['badge'][2]);
  }else{
    $qry->bindValue(':badge_id_3', NULL);
  }
  $qry->bindValue(':child_id', $_COOKIE['child_key']);
  $qry->execute();

} catch (PDOException $e) {
  echo 'DB接続エラー ： ' . $e->getMessage();
}

header('Location: mypage_child.php');