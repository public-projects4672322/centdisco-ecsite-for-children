<!DOCTYPE html>
<html lang="ja">

<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="stylesheet" href="css/reset.css">
  <link rel="stylesheet" href="css/style.css">
  <title>Cent Disco</title>
  <meta name="robots" content="none,noindex,nofollow">
  <style>
    .wrapper {
      min-height: 100vh;
      position: relative;
      /*←相対位置*/
      padding-bottom: 120px;
      /*←footerの高さ*/
      box-sizing: border-box;
      /*←全て含めてmin-height:100vhに*/
    }

    footer {
      position: absolute;
      /*←絶対位置*/
      bottom: 0;
      /*下に固定*/
    }
  </style>
</head>

<body class="body">
  <header class="header">
    <a href="index.php">
      <img src="images/logo001.png" alt="Cent Disco" class="header_logo">
    </a>
    <nav class="gnav">
      <ul class="menu">
        <li><a href="shop.php">Shop</a></li>
        <li><a href="login.php">MyPage&Login</a></li>
        <li><a href="contact.php">Contact</a></li>
        <li>
          <a href="cart.php">
            <img src="images/cart.png" alt="cart" class="header_cart">
          </a>
        </li>
      </ul>
    </nav>
  </header>
  <main class="main-content wrapper">
    <h1 class="body__title">現在準備中のページです</h1>

    <h2 style="text-align:center;font-size:24px;font-weight:normal;">実装まで今しばらくお待ちください</h2>

  </main>
  <footer class="footer" style="width:100%;">
    <p>&copy;Cent Disco</p>
  </footer>

</body>

</html>