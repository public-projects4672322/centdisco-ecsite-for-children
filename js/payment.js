var sum = 0;
var sum_now = 0;
var calc_temp;
var array = [];
var count1 = 0;
var count2 = 0;
var count3 = 0;
var count4 = 0;
var count5 = 0;
var count6 = 0;
var count7 = 0;
var count8 = 0;
var count9 = 0;

$(function () {
  $('.item').draggable({
    containment: '.container',
    opacity: 0.8,
  });
  $('.drop_area').droppable({
    containment: '.container',
    // アクティブなとき
    activate: function (e, ui) {
      $(this)
        .find("#status")
        .html("必要な金額を入れよう！");
    },
    // トレー上にあるとき
    over: function (e, ui) {
      $(this)
        .find("#status")
        .html("トレーの中に入れよう！");
    },
    // トレーから外れたとき
    out: function (e, ui) {
      // .drop_area > #status における文字変更
      $(this)
        .find("#status")
        .html("必要な金額を入れよう");
      // 配列に存在している場合、削除
      var idx = array.indexOf(ui.draggable.attr('id'));
      if (0 <= idx) {
        array.splice(idx, 1);
      }
      // トレー内(配列内)にある金額計算
      sum = 0;
      if (array.length) {
        for (key in array) {
          sum += Number($('#' + array[key]).attr('alt'));
        }
      }
      if (sum_now > sum) {
        cnt = sum_now - sum;
        switch (cnt) {
          case 10000:
            count1--;
            $('.count1')
              .val(count1);
            break;
          case 5000:
            count2--;
            $('.count2')
              .val(count2);
            break;
          case 1000:
            count3--;
            $('.count3')
              .val(count3);
            break;
          case 500:
            count4--;
            $('.count4')
              .val(count4);
            break;
          case 100:
            count5--;
            $('.count5')
              .val(count5);
            break;
          case 50:
            count6--;
            $('.count6')
              .val(count6);
            break;
          case 10:
            count7--;
            $('.count7')
              .val(count7);
            break;
          case 5:
            count8--;
            $('.count8')
              .val(count8);
            break;
          case 1:
            count9--;
            $('.count9')
              .val(count9);
            break;
          default:
            break;
        }
      }
      sum_now = sum;
      if (php.cart <= sum) {
        cent = sum - php.cart;
        // HTML上に金額表示
        amount = "お釣りは" + cent + "円です。";
        $('#amount')
          .html(amount);
        $('.cent')
          .val(cent);
      } else {
        cent = -1;
        amount = "支払い金額が足りていません。";
        $('#amount')
          .html(amount);
        $('.cent')
          .val(cent);
      }
      tray = "トレー内の金額は" + sum + "円です。";
      $('#sum')
        .html(tray);

    },
    // トレー上にドロップされたとき
    drop: function (e, ui) {
      // .drop_area > #status における文字変更
      $(this)
        .find("#status")
        .html("　");
      // 重複なしで配列に追加
      if (0 > array.indexOf(ui.draggable.attr('id'))) {
        array.push(ui.draggable.attr('id'));
      }
      // トレー内(配列内)にある金額計算
      sum = 0;
      if (array.length) {
        for (key in array) {
          sum += Number($('#' + array[key]).attr('alt'));
        }
      }
      if (sum_now < sum) {
        cnt = sum - sum_now;
        switch (cnt) {
          case 10000:
            count1++;
            $('.count1')
              .val(count1);
            break;
          case 5000:
            count2++;
            $('.count2')
              .val(count2);
            break;
          case 1000:
            count3++;
            $('.count3')
              .val(count3);
            break;
          case 500:
            count4++;
            $('.count4')
              .val(count4);
            break;
          case 100:
            count5++;
            $('.count5')
              .val(count5);
            break;
          case 50:
            count6++;
            $('.count6')
              .val(count6);
            break;
          case 10:
            count7++;
            $('.count7')
              .val(count7);
            break;
          case 5:
            count8++;
            $('.count8')
              .val(count8);
            break;
          case 1:
            count9++;
            $('.count9')
              .val(count9);
            break;
          default:
            break;
        }
      }
      sum_now = sum;
      if (php.cart <= sum) {
        cent = sum - php.cart;
        // HTML上に金額表示
        amount = "お釣りは" + cent + "円です。";
        $('#amount')
          .html(amount);
        $('.cent')
          .val(cent);
      } else {
        cent = -1;
        amount = "支払い金額が足りていません。";
        $('#amount')
          .html(amount);
        $('.cent')
          .val(cent);
      }
      tray = "トレー内の金額は" + sum + "円です。";
      $('#sum')
        .html(tray);
    }
  });
});