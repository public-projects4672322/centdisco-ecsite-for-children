<?php
session_start();
/*
ミッション1回でも達成の判定(missionテーブルにおいて)
・valid = no（回数指定なしのものを達成 または 指定回数分、すべて達成）
・valid = yes かつ default_count > count（1回 ～ default_count - count 回達成）
（countはミッション一回達成(承認)ごとに減算）
・valid = yes かつ default_count = null かつ count < 0（最低1回は達成）
上のいずれかの場合とする

countが0なら「valid = no」で承認済みとする(nullのときは×)
countが1以上ならcountから1だけ減算 (validはyesのまま）

例えばdefault_count = 3、 count = 1であれば、そのミッションは2回達成済み

回数指定が永続(permanent = yes, count = null)の場合は、どのような場合でもvalidがyesにはならない
count のマイナスの値が達成回数
例えばcount = -2であれば、永続のミッションを2回達成
*/

/*
ミッションを指定回数全て達成の判定
・valid = no（回数指定なしのものを達成 または 指定回数分、すべて達成）
(valid = yes かつ default_count is not null かつ count = 0になったら、valid = noにする)
*/

/*
ミッション失敗判定
・回数指定なし
    -> valid = yes かつ start_date + date_interval < 今日の日付 の場合
・回数指定有り
    -> 「valid = yes」 かつ、 「start_date + date_interval * default_count < 今日の日付 の時に、count > 0」の場合
    (何回成功し、何回失敗したか、だけを算出)
・永続
    -> 失敗判定無し、ただしミッション達成回数はcountに記載
    (count のマイナスの値が達成回数 例えばcount = -2であれば、永続のミッションを2回達成)
*/

/*
ミッションの承認回数制限について(回数指定有り/永続 のミッションのみ)
本来であればミッションテーブルにupdated_atとlocked(yes/no)のカラムを追加し、
    ・locked(no)がデフォルト
    ・locked(no)の時のみ承認可能
    ・ミッションが承認された際に、 n回目の開始日 < updated_at < n回目の終了日 のときに、locked(yes)する
    このことによってミッションの承認回数を制限する
    ・n+1回目の開始日 < updated_at のときに承認を押すと、locked(no)としてからcountを減算して、再度 n+1回目の開始日 < updated_at < n+1回目の終了日 のときはlocked(yes)する

つもりだったが、そこまで厳密にする必要性がないので(永続ミッション以外は、親アカウントに電子マネーを先にチャージさせてからミッションを出題しているはずなので、何回承認させても問題ない気がする)、理論上期間内に達成できる回数は連続で承認可能な仕組みにした
(達成可能な回数の最大値を算出)
ex)開始日2/2、期間1日、回数：4回のミッションであれば、
今日が2/4のときは最大で(2/2~2/3、2/3~2/4、2/4~2/5の)3回まで達成可能 -> この時点で承認回数制限は3回(もしそれ以前の日に達成してなくても、2/4の時点では3回まで承認可能)
*/

//ログイン判定
// setcookie(
//     "parent_key", //キー名称
//     "prnt001", //データ
//     time() + 60 * 60 * 2
// );
if (!isset($_COOKIE['parent_key'])) {
  //ログイン成功時のみcookieに保存される仕組みなので、dbのカラムの有無の判定は省略
  header('Location: index.php');
  exit;
}

//テーブル存在チェック関数
function table_exists($pdo, $table)
{
  $result = $pdo->query("SHOW TABLES LIKE '{$table}'");

  if ($result->rowCount() == 1) {
    return true;
  } else {
    return false;
  }
}

//カラム存在チェック関数
function column_exists($pdo, $table)
{
  $result = $pdo->query("SELECT * FROM $table /* WHERE deleted_at IS NULL */");

  if ($result->rowCount() > 0) {
    return true;
  } else {
    return false;
  }
}

//ミッション存在チェック関数
function mission_exists($pdo, $table, $mission_id)
{
  $result = $pdo->query("SELECT * FROM $table WHERE mission_id = $mission_id /* AND deleted_at IS NULL */");

  if ($result->rowCount() == 1) {
    return true;
  } else {
    return false;
  }
}

//ミッション存在チェック関数（ユーザー指定有り）
function mission_exists_child($pdo, $table, $child_id)
{
  $result = $pdo->query("SELECT * FROM $table WHERE child_id = '$child_id'/* AND deleted_at IS NULL */");

  if ($result->rowCount() > 0) {
    return true;
  } else {
    return false;
  }
}

//終了したミッション存在チェック関数（ユーザー指定有り）
function finished_mission($pdo, $table, $child_id)
{
  $result = $pdo->query("SELECT * FROM $table WHERE child_id = '$child_id' AND valid = 'no'/* AND deleted_at IS NULL */");

  if ($result->rowCount() > 0) {
    return true;
  } else {
    return false;
  }
}

// DB登録 //////////////////////////////////////////////////////////////////////////////////////
try {
  //DSN(Data Source Name)…DBへの接続情報
  $dsn = 'mysql:host=localhost;dbname=hew2022_it42107;charset=utf8mb4';
  $db_user = 'hew2022_it42107';
  $db_pass = '';

  //DB接続(open) イメージコマンドのmysql -u root -p xxx
  $pdo = new PDO($dsn, $db_user, $db_pass);

  //デフォルトの動作はエラー黙殺
  //→エラー時、そのアラートを発生するようにする
  $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

  //DB存在チェック(パーミッションがあること前提)
  $pdo->query("CREATE DATABASE IF NOT EXISTS hew2022_it42107");

  $dsn = 'mysql:host=localhost;dbname=hew2022_it42107;charset=utf8mb4';
  $pdo = new PDO($dsn, $db_user, $db_pass);
} catch (PDOException $e) {
  //簡易エラー処理
  echo $e->getMessage();

  //本来はログを出力し、エラーページなどに飛ばして終了の形が多い
  exit;
}

//ミッション承認
if (isset($_POST['confirm_mission'])) {
  //データ取得
  $sql = "SELECT * FROM mission WHERE mission_id = {$_POST['confirm_mission_id']}";
  $stmt = $pdo->prepare($sql);
  $stmt->execute();

  $mission_info = $stmt->fetch(PDO::FETCH_ASSOC);

  if ($_POST['confirm_permanent'] == 'yes' && $_POST['confirm_count'] == null) {
    //回数：永続(一回もミッション達成無しのとき)
    $sql = "UPDATE mission SET count = -1 WHERE mission_id = :mission_id";
  } else if ($_POST['confirm_permanent'] == 'yes') {
    //回数：永続(ミッション達成履歴ありの時)
    $sql = "UPDATE mission SET count = count - 1 WHERE mission_id = :mission_id";
  } else if ($_POST['confirm_count'] == null) {
    //回数指定なし
    $sql = "UPDATE mission SET valid = 'no' WHERE mission_id = :mission_id";
  } else if ($_POST['confirm_count'] >= 2) {
    //回数指定有り(ミッションがあと2回以上ある時)
    $sql = "UPDATE mission SET count = count - 1 WHERE mission_id = :mission_id";
  } else {
    //回数指定有り(ミッションがあと1回のある時)
    $sql = "UPDATE mission SET count = count - 1, valid = 'no' WHERE mission_id = :mission_id";
  }

  $prepare = $pdo->prepare($sql);
  $prepare->bindValue(':mission_id', $_POST['confirm_mission_id']);
  $prepare->execute();

  //レベルアップ
  // $result = $pdo -> query("SELECT * FROM level WHERE child_id = '{$mission_info['child_id']}'");
  // if( $result -> rowCount() == 1 ) {
  //     //levelテーブルに子供のカラムが存在する時
  //     $pdo -> query("UPDATE level SET level = level + {$mission_info['reward']} * 0.1 WHERE child_id = '{$mission_info['child_id']}'");
  // } else {
  //     //ないとき
  //     $pdo -> query("INSERT INTO level(child_id, level) VALUES('{$mission_info['child_id']}', {$mission_info['reward']} * 0.1)");
  // }

  $_SESSION['message_confirm'] = '【ID：' . $mission_info['mission_id'] . '】ミッション名：' . $mission_info['mission_name'] . ' のミッションが達成され、承認されました。';

  $_SESSION['flg_message'] = true;
  $_SESSION['flg_id'] = $mission_info['mission_id'];

  header("Location: mission_status.php#{$_POST['confirm_mission_id']}");
  exit;
}

//ミッション削除
if (isset($_POST['delete_mission'])) {
  //データ取得
  $sql = "SELECT * FROM mission WHERE mission_id = {$_POST['confirm_mission_id']}";
  $stmt = $pdo->prepare($sql);
  $stmt->execute();

  $mission_info = $stmt->fetch(PDO::FETCH_ASSOC);

  $pdo->query("DELETE FROM mission WHERE mission_id = {$_POST['confirm_mission_id']};");
  // $pdo -> query("UPDATE mission SET deleted_at = current_timestamp() WHERE mission_id = {$_POST['confirm_mission_id']};");

  $_SESSION['message_delete'] = '【ID：' . $mission_info['mission_id'] . '】ミッション名：' . $mission_info['mission_name'] . ' のミッションが削除されました。';

  header("Location: mission_status.php");
  exit;
}

//ミッション一括承認
$selected_missions = [];
if (isset($_POST['confirmAll']) && isset($_POST['select_mission']) && count($_POST['select_mission']) > 0) {
  $selected_missions = $_POST['select_mission'];
  // foreach($selected_missions as $m) {
  //     echo $m. '<br>';
  // }

  for ($i = 0; $i < count($selected_missions); $i++) {
    //データ取得
    $sql = "SELECT * FROM mission WHERE mission_id = {$selected_missions[$i]}";
    $stmt = $pdo->prepare($sql);
    $stmt->execute();

    $mission_info = $stmt->fetch(PDO::FETCH_ASSOC);

    if ($mission_info['permanent'] == 'yes' && $mission_info['count'] == null) {
      //回数：永続(一回もミッション達成無しのとき)
      $pdo->query("UPDATE mission SET count = -1 WHERE mission_id = {$selected_missions[$i]};");
    } else if ($mission_info['permanent'] == 'yes') {
      //回数：永続(ミッション達成履歴ありの時)
      $pdo->query("UPDATE mission SET count = count - 1 WHERE mission_id = {$selected_missions[$i]};");
    } else if ($mission_info['count'] == null) {
      //回数指定なし
      $pdo->query("UPDATE mission SET valid = 'no' WHERE mission_id = {$selected_missions[$i]};");
    } else if ($mission_info['count'] >= 2) {
      //回数指定有り(ミッションがあと2回以上ある時)
      $pdo->query("UPDATE mission SET count = count - 1 WHERE mission_id = {$selected_missions[$i]};");
    } else {
      //回数指定有り(ミッションがあと1回のある時)
      $pdo->query("UPDATE mission SET count = count - 1, valid = 'no' WHERE mission_id = {$selected_missions[$i]};");
    }

    $_SESSION['message_confirm'] .= '【ID：' . $mission_info['mission_id'] . '】ミッション名：' . $mission_info['mission_name'] . '<br>';
  }

  if ($i == 1) {
    $_SESSION['message_confirm'] .= 'このミッションが達成され、承認されました。';
  } else {
    $_SESSION['message_confirm'] .= 'これらのミッションが達成され、承認されました。';
  }

  header("Location: mission_status.php");
  exit;
} else if (isset($_POST['confirmAll']) && isset($_POST['select_mission']) && count($_POST['select_mission']) == 0) {
  $_SESSION['message_confirm'] = 'ミッションが選択されていません。';

  header("Location: mission_status.php");
  exit;
}

//ミッション一括削除
if (isset($_POST['deleteAll']) && isset($_POST['select_mission'])) {
  $selected_missions = $_POST['select_mission'];
  // foreach($selected_missions as $m) {
  //     echo $m. '<br>';
  // }

  for ($i = 0; $i < count($selected_missions); $i++) {
    //データ取得
    $sql = "SELECT * FROM mission WHERE mission_id = {$selected_missions[$i]}";
    $stmt = $pdo->prepare($sql);
    $stmt->execute();

    $mission_info = $stmt->fetch(PDO::FETCH_ASSOC);

    $pdo->query("DELETE FROM mission WHERE mission_id = {$selected_missions[$i]};");
    // $pdo -> query("UPDATE mission SET deleted_at = current_timestamp() WHERE mission_id = {$selected_missions[$i]};");

    $_SESSION['message_delete'] .= '【ID：' . $mission_info['mission_id'] . '】ミッション名：' . $mission_info['mission_name'] . '<br>';
  }

  if ($i == 1) {
    $_SESSION['message_delete'] .= 'このミッションが削除されました。';
  } else {
    $_SESSION['message_delete'] .= 'これらのミッションが削除されました。';
  }

  header("Location: mission_status.php");
  exit;
}
?>
<!DOCTYPE html>
<html lang="ja">

<head>
  <title>ミッションステータス</title>
  <meta charset="utf-8">
  <link rel="stylesheet" href="css/reset.css">
  <link rel="stylesheet" href="css/style.css">
  <script type="text/javascript">
    //ポップアップ
    function confirmMission() {
      var select = window.confirm("このミッションを承認します。「OK」を押下すると、このミッションを承認し、指定された報酬がお小遣いとしてお子様に与えられます。")
      return select
    }

    function deleteMission() {
      var select = window.confirm("このミッションを削除します。復元はできません。よろしいですか？")
      return select
    }

    function confirmAllMissions() {
      var select = window.confirm("選択された全てのミッションを承認します。「OK」を押下すると、これらのミッションを承認し、それぞれに指定された報酬がお小遣いとしてお子様に与えられます。")
      return select
    }

    function deleteAllMissions() {
      var select = window.confirm("選択された全てのミッションを削除します。復元はできません。よろしいですか？")
      return select
    }
  </script>
</head>

<body>
  <header class="header">
    <a href="index.php">
      <img src="images/logo001.png" alt="Cent Disco" class="header_logo">
    </a>
    <nav class="gnav">
      <ul class="menu">
        <li><a href="shop.php">Shop</a></li>
        <li><a href="login.php">MyPage&Login</a></li>
        <li><a href="contact.php">Contact</a></li>
        <li>
          <a href="cart.php">
            <img src="images/cart.png" alt="cart" class="header_cart">
          </a>
        </li>
      </ul>
    </nav>
  </header>
  <main class="main-content">
    <h1 class="body__title">ミッション - ステータス</h1>
    <dl class="form-content">
      <dt class="form-content__subtitle">01 ユーザー選択</dt>
      <dd class="form-content__input">
        <p style="margin-bottom: 10px;">ミッションを表示したいお子様のアカウントを選択し、確定ボタンを押下してください。</p>
        <!-- 子供のユーザー情報は存在すること前提 -->
        <form id="form1" action="mission_status.php" method="post">
          <select name="select_child" required style="width:60%; margin-bottom:40px;">
            <?php
            //データ取得
            // $sql = "SELECT * FROM children WHERE deleted_at IS NULL";
            $sql = "SELECT * FROM children WHERE parent_id = '{$_COOKIE['parent_key']}'";
            $stmt = $pdo->prepare($sql);
            $stmt->execute();

            $i = 0;
            while ($child = $stmt->fetch(PDO::FETCH_ASSOC)) {
              $children[] = $child;

              // foreach($children as $key => $value) {
              //     echo $key. '=>'. $value. '、';
              // }
              // echo "<br>";
            ?>

              <!-- 出力 -->
              <option value="<?php echo $children[$i]['child_id']; ?>"
              <?php
              if (isset($_SESSION['child_id']) && $_SESSION['child_id'] == $children[$i]['child_id']) {
              //確定ボタン押下後、初期選択
               echo 'selected';
              } else if (isset($_POST['select_child']) && $_POST['select_child'] == $children[$i]['child_id']) {
                //確定ボタン押下後、初期選択
                echo 'selected';
                //選択中のアカウント記憶
                $_SESSION['child_id'] = $_POST['select_child'];
              }?>>
                <?php echo htmlspecialchars($children[$i]['username']); ?>様 -
                ID：<?php echo htmlspecialchars($children[$i]['child_id']); ?> - お子様用アカウント</option>

            <?php $i++;
            } ?>
          </select>

          <input type="submit" value="確定" onclick="getValue()" style="width:10%;">
          <!-- <button onclick="formSubmit()">確定</button> -->
          <!-- </form> -->

          <?php
          //print_r($children);

          //ユーザー選択
          if (isset($_POST['select_child'])) {
            $child_id = $_POST['select_child'];
          } else if (isset($_SESSION['child_id'])) {
            $child_id = $_SESSION['child_id'];
          } else {
            //選択前(初期値)
            $child_id = $children[0]['child_id'];
          }

          //データ取得
          $sql = "SELECT * FROM children WHERE child_id = '$child_id'";
          $stmt = $pdo->prepare($sql);
          $stmt->execute();

          $child_info = $stmt->fetch(PDO::FETCH_ASSOC);
          // print_r($child_info);
          ?>

        <p style="margin-bottom:100px;">現在選択中のお子様のアカウント：<?php echo htmlspecialchars($child_info['username']); ?>様 -
            ID：<?php echo htmlspecialchars($child_info['child_id']); ?></p>
      </dd>
      <dt class="form-content__subtitle">02 ミッション選択</dt>
      <dd class="form-content__input">
      <p style="margin-bottom: 10px;">表示したいミッションのカテゴリを選択し、確定ボタンを押下してください。</p>
      <!-- <form id="mission_form" action="mission_status.php" method="post"> -->
      <select name="select_category" style="width:60%; margin-bottom:40px;">
        <option value="enable" <?php
                                if (isset($_POST['select_category']) && $_POST['select_category'] == 'enable') {
                                  echo 'selected';
                                }
                                ?>>出題中のミッション(期限切れで未達成となったミッションを含みます。永続のミッションはこちらを選択)</option>
        <option value="finished" <?php
                                  if (isset($_POST['select_category']) && $_POST['select_category'] == 'finished') {
                                    echo 'selected';
                                  }
                                  ?>>達成済みのミッション(永続のミッションは含まれません)</option>
      </select>
      <input type="submit" value="確定" style="width:10%;">
      <!-- <button onclick="formSubmit()">確定</button> -->
      </form>
      </dd>
      <dt class="form-content__subtitle" style="margin-bottom:1oopx;">03 ミッション一覧</dt>
      <dd class="form-content__input">
      <?php
      //子供アカウント選択前
      // $child_id = $children[0]['child_id'];

      //承認完了メッセージ出力
      if (isset($_SESSION['message_confirm'])) { ?>
        <font color="blue"><?php echo $_SESSION['message_confirm']; ?></font>
      <?php }

      //削除完了メッセージ出力
      if (isset($_SESSION['message_delete'])) { ?>
        <font color="red"><?php echo $_SESSION['message_delete']; ?></font>
      <?php }

      //アラートメッセージ出力
      if (isset($_SESSION['message_alert'])) { ?>
        <font color="red" style="font-weight: bold;"><?php echo $_SESSION['message_alert']; ?></font>
      <?php }

      if (table_exists($pdo, 'mission') && mission_exists_child($pdo, 'mission', $child_id)) {
        $message = '現在お子様に出題されているミッション一覧です。';
        if (isset($_POST['select_category']) && $_POST['select_category'] == 'finished' && finished_mission($pdo, 'mission', $child_id)) {
          $message = 'お子様が達成したミッション一覧です。';
        } else if (isset($_POST['select_category']) && $_POST['select_category'] == 'finished') {
          $message = '指定されたミッションが存在しません。';
        }
      ?>
        <p><?php echo $message; ?></p>
      </dd>
        <?php
        //データ取得
        // $sql = "SELECT * FROM children WHERE deleted_at IS NULL";
        // $sql = "SELECT * FROM mission WHERE child_id = '$child_id' AND valid = 'yes'";

        //02ミッション選択の内容に応じて
        $sql_category = "SELECT * FROM mission WHERE child_id = '$child_id' AND valid = 'yes'";
        if (isset($_POST['select_category']) && $_POST['select_category'] == 'finished') {
          $sql_category = "SELECT * FROM mission WHERE child_id = '$child_id' AND valid = 'no'";
        }
        $stmt = $pdo->prepare($sql_category);
        $stmt->execute();

        $i = 0;
        ?>

        <form action="mission_status.php" method="post" style="margin: auto;">
          <?php
          while ($mission = $stmt->fetch(PDO::FETCH_ASSOC)) {
            $missions[] = $mission;

            // foreach($missions as $key => $value) {
            //     echo $key. '=>'. $value. '、';
            // }
            // echo "<br>";
          ?>






            <!-- 出力 -->
            <div id="<?php echo $missions[$i]['mission_id']; ?>" style="width:100%;margin:auto;">
              <label>
                <p style="padding: 10px; margin-bottom: 10px; border: 1px solid #c0c0c0;border-radius:15px;width:20px;display:inline-block;"><input id="<?php echo $i; ?>" type="checkbox" name="select_mission[]" form="missions" value="<?php echo $missions[$i]['mission_id']; ?>"></p>
                <?php
                // if (isset($_POST['unCheckAll'])) {
                //  echo '';
                // } else if (isset($_POST['checkAll'])) {
                //   echo 'checked';
                // }
                $m = '';
              	//承認回数制限時や、失敗したミッションを選択不可に
              if ($missions[$i]['permanent'] == 'yes') {
                //回数永続
                //今日
                $date_2 = new DateTime();
                // $date = new DateTime('2022-08-30');
                //開始日
                $start_date_2 = new DateTime($missions[$i]['start_date']);
								//日数差
								$diff_2 = $date_2->diff($start_date_2);
								$interval_2 = $diff_2->days;
								//承認回数制限
								if ($missions[$i]['count'] != null && (floor($interval_2 / $missions[$i]['date_interval'])) < 0 - $missions[$i]['count']) {
									$m = 'disabled';
								}
							} else if ($missions[$i]['default_count'] != null) {
								//回数指定有り
								//今日
								$date_2 = new DateTime();
								// $date = new DateTime('2022-08-30');
								//開始日
								$start_date_2 = new DateTime($missions[$i]['start_date']);
								//日数差
								$diff_2 = $date_2->diff($start_date_2);
								$interval_2 = $diff_2->days;
								//承認回数制限
								if ((floor($interval_2 / $missions[$i]['date_interval'])) < $missions[$i]['default_count'] - $missions[$i]['count']) {
									$m = 'disabled';
								}
							}
							//失敗したミッション
							if ($missions[$i]['permanent'] == 'no' && $missions[$i]['default_count'] == null) {
								//回数指定なし
								//今日と次回終了日の日数差
								$date_3 = new DateTime();
								$start_date_3 = new DateTime($missions[$i]['start_date']);
								$diff_3 = $date_3->diff(new DateTime($start_date_3->modify("+" . $missions[$i]['date_interval'] . 'days')->format('Y-m-d')));
								if (!isset($_POST['select_category']) || (isset($_POST['select_category']) && $_POST['select_category'] == 'enable')) {
									if (($diff_3->format('%R') . $diff_3->days) < 0) {
										$m = 'disabled';
									}
								}
							} else if ($missions[$i]['permanent'] == 'no' && $missions[$i]['default_count'] != null) {
								//回数指定有り
								//最終的な終了日
                $date_4 = new DateTime();
                $start_date_4 = new DateTime($missions[$i]['start_date']);
								$scheduled_end_date_4 = $start_date_4->modify("+" . $missions[$i]['date_interval'] * $missions[$i]['default_count'] . 'days');
                //日数差
              	$diff_4 = $date_4->diff($start_date_4);
                $interval_4 = $diff_4->days;
                //開始日 + (日数差÷期間の商 + 1) × 期間
                $next_end_date_4 = $start_date_4->modify("+" . (floor($interval_4 / $missions[$i]['date_interval']) + 1) * $missions[$i]['date_interval'] . 'days')->format('Y年n月j日'); //日数差0の時にも対応するためceilにはしない
								if ($scheduled_end_date_4 < $next_end_date_4 && $date_4 != $scheduled_end_date_4) {
                	$m = 'disabled';
                }
								//最終回の時に、残りの日数表示
                if ($missions[$i]['default_count'] == floor($interval_4 / $missions[$i]['date_interval']) + 1 || $date_4 == $scheduled_end_date_4) {
                //今日と次回終了日の日数差
                $date_5 = new DateTime();
                $start_date_5 = new DateTime($missions[$i]['start_date']);
                $diff_5 = $date_5->diff($scheduled_end_date_4);
								// echo $diff2 -> format('%R'). $diff2 -> days;
                if (($diff_5->format('%R') . $diff_5->days) < 0) {
                $m = 'disabled';
                }
                }
                $date_6 = new DateTime();
                $start_date_6 = new DateTime($missions[$i]['start_date']);
                $scheduled_end_date_6 = $start_date_6->modify("+" . $missions[$i]['date_interval'] * $missions[$i]['default_count'] . 'days');
                $diff_6 = $date_6->diff($scheduled_end_date_6);
                if (($diff_6->format('%R') . $diff_6->days) < 0) {
                $m = 'disabled';
                }
                }
                echo $m;
                ?> 【ID：<?php echo htmlspecialchars($missions[$i]['mission_id']); ?>】ミッション名：<?php echo htmlspecialchars($missions[$i]['mission_name']); ?></p>
              </label>
              <p style="padding: 10px; margin-bottom: 10px; border: 1px solid #c0c0c0;border-radius:15px;">ミッション詳細：
							<?php
              if ($missions[$i]['mission_detail'] == '') {
              	echo '【設定なし】';
              } else {
                echo htmlspecialchars($missions[$i]['mission_detail']);
              }
              ?></p>
              <br>

              <div>
                <?php
                //承認完了メッセージ出力
                if (isset($_SESSION['message_confirm']) && isset($_SESSION['flg_message']) && isset($_SESSION['flg_id']) && $missions[$i]['mission_id'] == $_SESSION['flg_id']) { ?>
                  <font color="blue"><?php echo $_SESSION['message_confirm']; ?></font>
                <?php } ?>

                <br>
                開始日：<?php
                    $start_date = (new DateTime($missions[$i]['start_date']))->format('Y年n月j日');
                    //曜日
                    $weekday = ['(日)', '(月)', '(火)', '(水)', '(木)', '(金)', '(土)'];
                    $w = $weekday[(new DateTime($missions[$i]['start_date']))->format('w')];
                    $start_date .= $w;

                    echo $start_date; ?>　

                期間：<?php echo $missions[$i]['date_interval']; ?>日後に終了　

                報酬：￥<?php echo $missions[$i]['reward']; ?>　

                <?php
                if ($missions[$i]['default_count'] != null) {
                  echo '回数：' . $missions[$i]['default_count'] . '回　';
                } else if ($missions[$i]['permanent'] == 'yes') {
                  echo '回数：【永続】　';
                } ?>

                <?php
                $next_end_date = new DateTime();

                //今日
                $date = new DateTime();
                // $date = new DateTime('2022-08-30');
                //開始日
                $start_date = new DateTime($missions[$i]['start_date']);

                //日数差
                $diff = $date->diff($start_date);
                $interval = $diff->days;

                $failed = false;
                $flg_accept = false;
                if ($missions[$i]['permanent'] == 'yes') {
                  //回数永続

                  //その日時点での達成回数
                  error_reporting(0);
                  if ($missions[$i]['count'] != null) {
                    echo '現時点での達成回数：' . 0 - (int)$missions[$i]['count'] . '回';
                  } else {
                    echo '【このミッションには達成実績がありません。】';
                  }

                  //次回の開始日
                  //開始日 + (日数差÷期間の商 + 1) × 期間 - 期間
                  $next_start_date = $start_date->modify("+" . (floor($interval / $missions[$i]['date_interval']) + 1) * $missions[$i]['date_interval'] - $missions[$i]['date_interval'] . 'days')->format('Y年n月j日'); //日数差0の時にも対応するためceilにはしない

                  //次回の終了日
                  $start_date = new DateTime($missions[$i]['start_date']);
                  //開始日 + (日数差÷期間の商 + 1) × 期間
                  $next_end_date = $start_date->modify("+" . (floor($interval / $missions[$i]['date_interval']) + 1) * $missions[$i]['date_interval'] . 'days')->format('Y年n月j日'); //日数差0の時にも対応するためceilにはしない

                  //承認回数制限
                  if ($missions[$i]['count'] != null && (floor($interval / $missions[$i]['date_interval'])) < 0 - $missions[$i]['count']) {
                    $flg_accept = true;
                ?>
                    <input type="hidden" name="flg_accept" form="missions" value="true">
                  <?php
                  }

                  echo '<br>今回（' . (floor($interval / $missions[$i]['date_interval']) + 1) . '回目）のミッション開始日：' . $next_start_date;
                  echo '<br>今回（' . (floor($interval / $missions[$i]['date_interval']) + 1) . '回目）のミッション終了日：' . $next_end_date;
                } else if ($missions[$i]['default_count'] == null) {
                  //回数指定なし

                  //開始日 + 期間
                  $next_end_date = $start_date->modify("+" . $missions[$i]['date_interval'] . 'days')->format('Y年n月j日');

                  //出力
                  // $next_end_date .= '(本日の日付：'. $date -> format('Y年n月j日'). ')';
                  echo '<br>ミッション終了日：' . $next_end_date;

                  //今日と次回終了日の日数差
                  $start_date = new DateTime($missions[$i]['start_date']);
                  $diff2 = $date->diff(new DateTime($start_date->modify("+" . $missions[$i]['date_interval'] . 'days')->format('Y-m-d')));

                  // echo $diff2 -> format('%R'). $diff2 -> days;
                  if (!isset($_POST['select_category']) || (isset($_POST['select_category']) && $_POST['select_category'] == 'enable')) {
                    if (($diff2->format('%R') . $diff2->days) < 0) {
                      //ミッション失敗
                      $failed = true;
                    } else if (($diff2->days) == 0) {
                      echo '<font color="red">【このミッションは本日で終了します】</font>';
                    } else {
                      echo '<font color="blue">【このミッションは' . $diff2->days . '日後に終了します】</font>';
                    }
                  }
                } else {
                  //回数指定有り

                  //その日時点での達成回数
                  if ($missions[$i]['default_count'] > $missions[$i]['count']) {
                    echo '現時点での達成回数：' . ((int)$missions[$i]['default_count'] - (int)$missions[$i]['count']) . '回';
                  } else {
                    echo '【このミッションには達成実績がありません。】';
                  }

                  //最終的な終了日
                  $start_date = new DateTime($missions[$i]['start_date']);
                  $scheduled_end_date = $start_date->modify("+" . $missions[$i]['date_interval'] * $missions[$i]['default_count'] . 'days');

                  //次回の終了日
                  $start_date = new DateTime($missions[$i]['start_date']);
                  //開始日 + (日数差÷期間の商 + 1) × 期間
                  $next_end_date = $start_date->modify("+" . (floor($interval / $missions[$i]['date_interval']) + 1) * $missions[$i]['date_interval'] . 'days'); //日数差0の時にも対応するためceilにはしない

                  if ($scheduled_end_date >= $next_end_date) {
                    //次回の開始日
                    $start_date = new DateTime($missions[$i]['start_date']);
                    //開始日 + (日数差÷期間の商 + 1) × 期間 - 期間
                    $next_start_date = $start_date->modify("+" . (floor($interval / $missions[$i]['date_interval']) + 1) * $missions[$i]['date_interval'] - $missions[$i]['date_interval'] . 'days')->format('Y年n月j日'); //日数差0の時にも対応するためceilにはしない

                    //次回の終了日
                    $next_end_date = $next_end_date->format('Y年n月j日');

                    echo '<br>今回（' . (floor($interval / $missions[$i]['date_interval']) + 1) . '回目）のミッション開始日：' . $next_start_date;
                    echo '<br>今回（' . (floor($interval / $missions[$i]['date_interval']) + 1) . '回目）のミッション終了日：' . $next_end_date;
                  } else if ($date != $scheduled_end_date) {
                    //ミッション失敗
                    $failed = true;
                    $_SESSION['flg_accept'] = true;
                  }

                  //最終回の時に、残りの日数表示
                  if ($missions[$i]['default_count'] == floor($interval / $missions[$i]['date_interval']) + 1 || $date == $scheduled_end_date) {
                    //今日と次回終了日の日数差
                    $start_date = new DateTime($missions[$i]['start_date']);
                    $diff2 = $date->diff($scheduled_end_date);

                    // echo $diff2 -> format('%R'). $diff2 -> days;
                    if (($diff2->format('%R') . $diff2->days) < 0) {
                      //ミッション失敗
                      $failed = true;
                    } else if (($diff2->days) == 0) {
                      echo '<font color="red">【このミッションは本日で終了します】</font>';
                    } else {
                      echo '<font color="blue">【このミッションは' . $diff2->days . '日後に終了します】</font>';
                    }
                  }

                  //承認回数制限
                  if ((floor($interval / $missions[$i]['date_interval'])) < $missions[$i]['default_count'] - $missions[$i]['count']) {
                    $flg_accept = true;
                  ?>
                    <input type="hidden" name="flg_accept" form="missions" value="true">
                <?php
                  }

                  $diff3 = $date->diff($scheduled_end_date);
                  // echo $diff3 -> format('%R'). $diff3 -> days;

                  if (($diff3->format('%R') . $diff3->days) < 0) {
                    //ミッション失敗
                    $failed = true;
                  }
                  echo '<br>最終的な終了日：' . $scheduled_end_date->format('Y年n月j日');
                }
                ?>

                <?php
                if ($failed && (!isset($_POST['select_category']) || (isset($_POST['select_category']) && $_POST['select_category'] == 'enable'))) {
                  echo '<br><font color="red" style="font-weight: bold;">！このミッションは期限切れです。</font>';
                }
                ?>
              </div>

              <!-- ミッション編集、承認 -->
              <?php
              if (!isset($_POST['select_category']) || (isset($_POST['select_category']) && $_POST['select_category'] == 'enable')) {
              ?>
                <form method="post" name="edit_confirm" action="mission_status.php">
                  <input type="submit" name="delete_mission" value="削除" onclick="return deleteMission()">

                  <a href="mission_post.php?&mission_id=<?php echo $missions[$i]['mission_id']; ?>">編集/再出題</a>

                  <input type="hidden" name="confirm_mission_id" value="<?php echo $missions[$i]['mission_id']; ?>">
                  <input type="hidden" name="confirm_default_count" value="<?php echo $missions[$i]['default_count']; ?>">
                  <input type="hidden" name="confirm_count" value="<?php echo $missions[$i]['count']; ?>">
                  <input type="hidden" name="confirm_permanent" value="<?php echo $missions[$i]['permanent']; ?>">

                  <?php
                  if ($flg_accept) {
                    //指定の期間内にミッション達成済みのとき
                    echo 'このミッションは期間内に達成済みです。次回のミッション開始日まで承認はできません。';
                  } else if ($failed) {
                    echo 'このミッションは期限切れです。承認はできません。';
                  } else {
                    //指定の期間内にミッションが未達成の時
                  ?>
                    <input type="submit" name="confirm_mission" value="承認" onclick="return confirmMission()">
                  <?php } ?>
                </form>
              <?php } else { ?>
                <a href="mission_post.php?&mission_id=<?php echo $missions[$i]['mission_id']; ?>">編集/再出題</a>
              <?php }

              if (($missions[$i]['count'] != null && $missions[$i]['default_count'] == null) || $missions[$i]['default_count'] > $missions[$i]['count']) {
                //ミッション達成回数が一回でもある場合のみ表示(永続 or 回数指定有り)
              ?>
                <p>※回数の指定があるミッションを編集した場合、そのミッションの「現時点での達成回数」はリセットされます</p>
              <?php } ?>

              <hr>
            </div>

          <?php $i++;
          }
          // print_r($products);
          ?>

          <?php
          // if (!isset($_POST['select_category']) || (isset($_POST['select_category']) && $_POST['select_category'] == 'enable')) { ?>
            <!-- <form action="mission_status.php" method="post">
              <input type="submit" name="checkAll" value="すべて選択">
              <input type="submit" name="unCheckAll" value="すべて選択解除">
            </form><br>

            <form action="mission_status.php" id="missions" method="post">
              <input type="submit" name="deleteAll" onclick="return deleteAllMissions()" value="選択したミッションの一括削除">
              <input type="submit" name="confirmAll" onclick="return confirmAllMissions()" value="選択したミッションの一括承認">
            </form>

            ※承認ボタンの表示されていないミッションは選択することができません。個別に削除してください。 -->
          <?php //} ?>

        </form>
      <?php } else { ?>
        <p>指定されたミッションが存在しません。</p>
      <?php } ?>

      <br>
      <a href="mission_post.php">
        <button class="button">ミッションを追加する</button>
      </a>
      <a href="mypage_child.php">
        <button class="button">マイページへ</button>
      </a>
    </dl>

    <footer class="footer">
      <p>&copy;Cent Disco</p>
    </footer>
  </main>
</body>

</html>

<?php
//ページのリロード時にエラーメッセージ消去
//そのためにセッションを破棄

// // セッション変数を全て解除する
// $_SESSION = array();

// // セッションを切断するにはセッションクッキーも削除する。
// // Note: セッション情報だけでなくセッションを破壊する。
// if (ini_get("session.use_cookies")) {
//     $params = session_get_cookie_params();
//     setcookie(session_name(), '', time() - 42000,
//         $params["path"], $params["domain"],
//         $params["secure"], $params["httponly"]
//     );
// }

// // 最終的に、セッションを破壊する
// session_destroy();

unset($_SESSION['message_confirm']);
unset($_SESSION['flg_message']);
unset($_SESSION['flg_id']);

unset($_SESSION['message_delete']);

unset($_SESSION['message_alert']);
