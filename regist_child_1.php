<?php
/*
if (isset($_POST['parent_id'])) {
	require_once('regist_child_confirm_2.php');
}
*/
if(isset($_COOKIE['child_id'])){
	$child_id = $_COOKIE['child_id'];
}else{
	$child_id = "";
}


session_start();
$error_message = '';
if (isset($_SESSION['error'])) {
	$error_message =$_SESSION['error'];
	unset($_SESSION['error']);
}

if(isset($_COOKIE['err_child_id'])){
	$err_child_id = $_COOKIE['err_child_id'];
	setcookie('err_child_id', '', time() - 60);
}else{
	$err_child_id = "";
}

?>
<!DOCTYPE html>
<html lang="jp">
<head>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="robot" content="none, noindex, nofollow">
	<link rel="stylesheet" href="css/reset.css">
	<link rel="stylesheet" href="css/style.css">
	<title>利用者会員登録</title>
	<meta name="robots" content="none,noindex,nofollow">
</head>
<body>
	<header class="header">
		<a href="index.php">
			<img src="images/logo001.png" alt="Cent Disco" class="header_logo">
		</a>
		<nav class="gnav">
			<ul class="menu">
				<li><a href="shop.php">Shop</a></li>
				<li><a href="login.php">MyPage&Login</a></li>
				<li><a href="contact.php">Contact</a></li>
				
				<li>
					<a href="cart.php">
						<img src="images/cart.png" alt="cart" class="header_cart">
					</a>
				</li>
			</ul>
		</nav>
	</header>
	<main class="main-content">
		<h2 class="body__title">SignUp-子供会員登録</h2>

		<output name="result" style="color:red"><?php  echo $error_message;?></output><br>
		<output name="result" style="color:red"><?php  echo $err_child_id;?></output><br>

		<form class="form-content" action="regist_child_2.php" method="post" >
			<div class="form-content__subtitle">ユーザID(半角英数8文字以下)<font color="red">　必須</font></div>
			<div class="form-content__input">
				<input type="text" name="child_id" value="<?php echo $child_id;?>">
			</div>

			<div class="form-content__subtitle">パスワード<font color="red">　必須</font></div>
			<div class="form-content__input">
				<input type="password" name="password_1">
			</div>

			<div class="form-content__subtitle">パスワード再入力<font color="red">　必須</font></div>
			<div class="form-content__input">
				<input type="password" name="password">
			</div>

			<div class="form-content__submit"><input type="submit" value="次へ"></div>
			</from>

	<footer class="footer">
 		<p>&copy;Cent Disco</p>
	</footer>
</body>
</html>
