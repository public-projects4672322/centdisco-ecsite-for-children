<?php
session_start();
$message = '';
if (isset($_SESSION['message'])) {
    $message = $_SESSION['message'];
    unset($_SESSION['message']);
}
if(isset($_COOKIE['child_key'])){
  header('Location: mypage_child.php');
}
if(isset($_COOKIE['parent_key'])){
  header('Location: mypage_parent.php');
}
?><!DOCTYPE html>
<html lang="ja">
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="stylesheet" href="css/reset.css">
  <link rel="stylesheet" href="css/style.css">
  <title>Cent Disco | Login</title>
  <meta name="robots" content="none,noindex,nofollow">
</head>
<body class="body">
  <header class="header">
    <a href="index.php">
        <img src="images/logo001.png" alt="Cent Disco" class="header_logo">
    </a>
    <nav class="gnav">
        <ul class="menu">
            <li><a href="shop.php">Shop</a></li>
            <li><a href="login.php">MyPage&Login</a></li>
            <li><a href="contact.php">Contact</a></li>
            
            <li>
              <a href="cart.php">
                <img src="images/cart.png" alt="cart" class="header_cart">
            </a>
            </li>
        </ul>
    </nav>
  </header>
  <main class="main-content">
    <h2 class="body__title">Cent Disco にログイン</h2>
    <dl class="form-content">
      <dt class="form-content__title">Top - Log In</dt>
      <dd class="form-content__description">
        <p>Cent Discoのログインページです。<br>
          保護者用のアカウント情報が入力されたら保護者用のマイページへ、子供用であれば子供用のページへ遷移します。<br>
          会員登録していない方は、下のボタンもしくはここのリンクから<a href="#" style="border-bottom:1px solid #000;">会員登録</a>をしてください。<br>
          保護者用、利用者用ともに登録が必要です。<br>
          IDやパスワードをお忘れの方は下のボタンへ。
        </p>
      </dd>
      <!-- ログイン成功か失敗かの処理 -->
      <output style="color:red"><?php echo $message;?></output>
      <form class="form-content__form" action="login_check.php" method="post">
        <dt class="form-content__subtitle">01 - Attribute</dt>
        <dd class="form-content__radio">
          <input type="radio" name="access" value="1" id="child" checked><label for="child">子供</label>
          <input type="radio" name="access" value="2" id="parent"><label for="parent">保護者</label>
        </dd>
        <dt class="form-content__subtitle">02 - ID</dt>
        <dd class="form-content__input"><input type="text" name="id" id="id" value="hew2022"></dd>
        <dt class="form-content__subtitle">03 - Password</dt>
        <dd class="form-content__input"><input type="password" name="password" id="password" value="hew2022"></dd>
        <dd class="form-content__submit"><input type="submit" value="ログイン"></dd>
      </form>
      <dd class="link-button"><a href="regist_parent_1.php">会員登録はこちら</a></dd>
      <dd class="link-button"><a href="">ID、パスワードを忘れた方はこちら</a></dd>
    </dl>
  </main>

  <footer class="footer">
    <p>&copy;Cent Disco</p>
  </footer>
</body>

</html>