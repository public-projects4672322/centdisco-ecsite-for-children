<?php
session_start();
if ($_POST['a'] == '' ||
$_POST['b'] == '' ||
$_POST['c'] == '' ||
$_POST['d'] == ''){
$_SESSION['error'] = '※の項目に空白があります';
header('Location: child_info_add002.php?key=data&parent_id='.$_POST['parent_id'].'child_id='.$_POST['child_id'].'&password='.$_POST['password']);
exit;
}
?>
<!DOCTYPE html>
<html lang="jp">

<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="robot" content="none, noindex, nofollow">
  <link rel="stylesheet" href="css/reset.css">
  <link rel="stylesheet" href="css/style.css">
  <title>利用者会員登録確認画面</title>
  <script src="https://ajaxzip3.github.io/ajaxzip3.js" charset="UTF-8"></script>
  <meta name="robots" content="none,noindex,nofollow">
</head>
<body>
  <header class="header">
    <a href="index.php">
      <img src="images/logo001.png" alt="Cent Disco" class="header_logo">
    </a>
    <nav class="gnav">
      <ul class="menu">
        <li><a href="shop.php">Shop</a></li>
        <li><a href="login.php">MyPage&Login</a></li>
        <li><a href="contact.php">Contact</a></li>
        
        <li>
          <a href="cart.php">
            <img src="images/cart.png" alt="cart" class="header_cart">
          </a>
        </li>
      </ul>
    </nav>
  </header>

  <main class="main-content">
    <h2 class="body__title">SignUp-利用者会員登録確認画面</h2>
    <form class="form-content" method="post" action="child_info_add_complete.php">

      <div class="form-content__subtitle">姓、名(漢字)<font color="red">　必須</font></div>
      <div class="form-content__input">
        <div class="">
          <input type="text" name="a" value="<?php echo $_POST['a'];?>" disabled>
          <input type="text" name="b" value="<?php echo $_POST['b'];?>" disabled>
        </div>
      </div>

      <div class="form-content__subtitle">姓、名(カナ)<font color="red">　必須</font></div>
      <div class="form-content__input">
        <div class="">
          <input type="text" name="c" value="<?php echo $_POST['c'];?>" disabled>
          <input type="text" name="d" value="<?php echo $_POST['d'];?>" disabled>
        </div>
      </div>

      <div class="form-content__subtitle">メールアドレス</div>
      <div class="form-content__input">
        <input type="email" name="mail" size="30" maxlength="50" value="<?php echo $_POST['mail'];?>" disabled>
      </div>

      <div class="form-content__subtitle">郵便番号</div>
      <div class="form-content__input">
        <input type="text" name="zip01" size="10" maxlength="8" onKeyUp="AjaxZip3.zip2addr(this,'','pref01','addr01');" value="<?php echo $_POST['zip01'];?>" disabled>
      </div>

      <div class="form-content__subtitle">都道府県</div>
      <div class="form-content__input">
        <input type="text" name="pref01" size="20" value="<?php echo $_POST['pref01'];?>" disabled>
      </div>

      <div class="form-content__subtitle">市区町村</div>
      <div class="form-content__input">
        <input type="text" name="addr01" size="60" value="<?php echo $_POST['addr01'];?>" disabled>
      </div>

      <div class="form-content__subtitle">丁目 - 番地</div>
      <div class="form-content__input">
        <input type="text" name="q" size="60"value="<?php echo $_POST['q'];?>" disabled>
      </div>

      <div class="form-content__subtitle">建物名</div>
      <div class="form-content__input">
        <input type="text" name="w" size="60" value="<?php echo $_POST['w'];?>" disabled>
      </div>

      <input type="hidden" name="parent_id" value="<?php echo $_POST['parent_id']; ?>">
      <input type="hidden" name="child_id" value="<?php echo $_POST['child_id']; ?>">
      <input type="hidden" name="password" value="<?php echo $_POST['password']; ?>">
      <input type="hidden" name="child_name" value="<?php echo $_POST['a']; echo $_POST['b'];?>">
      <input type="hidden" name="child_name_kana" value="<?php echo $_POST['c']; echo $_POST['d'];?>">

      <div class="form-content__submit_a">
        <a href="child_info_add001.php"><input type="button" value="登録(1/2)へ戻る"></a>
        <a href="child_info_add002.php?key=data&parent_id=<?php echo $_POST['parent_id']; ?>&child_id=<?php echo $_POST['child_id']; ?>&password=<?php echo $_POST['password']; ?>"><input type="button" value="登録(2/2)へ戻る"></a></div>
        <div class="form-content__submit"><input type="submit" value="次へ"></div>
      </form>
      
      <footer class="footer">
        <p>&copy;Cent Disco</p>
      </footer>
    </body>

    </html>
