<?php
session_start();

setcookie('parent_name', $_POST['parent_name'], time() + 60 * 30);
setcookie('mail', $_POST['mail'], time() + 60 * 30);
setcookie('zip01', $_POST['zip01'], time() + 60 * 30);
setcookie('pref01', $_POST['pref01'], time() + 60 * 30);
setcookie('addr01', $_POST['addr01'], time() + 60 * 30);
setcookie('house_number', $_POST['house_number'], time() + 60 * 30);
setcookie('building_name', $_POST['building_name'], time() + 60 * 30);


if ($_POST['parent_name'] == '' ||
$_POST['mail'] == '' ||
$_POST['zip01'] == '' ||
$_POST['pref01'] == '' ||
$_POST['addr01'] == '' ||
$_POST['house_number'] == ''){
$_SESSION['error'] = '必須項目に空白があります';
header('Location: regist_parent_2.php');
exit;
}

?>
<!DOCTYPE html>
<html lang="jp">

<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="robot" content="none, noindex, nofollow">
  <link rel="stylesheet" href="css/reset.css">
  <link rel="stylesheet" href="css/style.css">
  <title>保護者会員登録確認画面</title>
  <script src="https://ajaxzip3.github.io/ajaxzip3.js" charset="UTF-8"></script>
  <meta name="robots" content="none,noindex,nofollow">
</head>
<body>
  <header class="header">
    <a href="index.php">
      <img src="images/logo001.png" alt="Cent Disco" class="header_logo">
    </a>
    <nav class="gnav">
      <ul class="menu">
        <li><a href="shop.php">Shop</a></li>
        <li><a href="login.php">MyPage&Login</a></li>
        <li><a href="contact.php">Contact</a></li>
        
        <li>
          <a href="cart.php">
            <img src="images/cart.png" alt="cart" class="header_cart">
          </a>
        </li>
      </ul>
    </nav>
  </header>

  <main class="main-content">
    <h2 class="body__title">SignUp-保護者会員登録確認画面</h2>
    <form class="form-content" method="post" action="regist_child_1.php">

      <div class="form-content__subtitle">姓名(漢字)<font color="red">　必須</font></div>
      <div class="form-content__input">
        <div class="">
          <input type="text" name="a" value="<?php echo $_POST['parent_name'];?>" disabled>
        </div>
      </div>

      <div class="form-content__subtitle">メールアドレス<font color="red">　必須</font></div>
      <div class="form-content__input">
        <input type="email" name="mail" size="30" maxlength="50" value="<?php echo $_POST['mail'];?>" disabled>
      </div>

      <div class="form-content__subtitle">郵便番号(ハイフンなし)<font color="red">　必須</font></div>
      <div class="form-content__input">
        <input type="text" name="zip01" size="10" maxlength="8" onKeyUp="AjaxZip3.zip2addr(this,'','pref01','addr01');" value="<?php echo $_POST['zip01'];?>" disabled>
      </div>

      <div class="form-content__subtitle">都道府県<font color="red">　必須</font></div>
      <div class="form-content__input">
        <input type="text" name="pref01" size="20" value="<?php echo $_POST['pref01'];?>" disabled>
      </div>

      <div class="form-content__subtitle">市区町村<font color="red">　必須</font></div>
      <div class="form-content__input">
        <input type="text" name="addr01" size="60" value="<?php echo $_POST['addr01'];?>" disabled>
      </div>

      <div class="form-content__subtitle">丁目 - 番地<font color="red">　必須</font></div>
      <div class="form-content__input">
        <input type="text" name="q" size="60" value="<?php echo $_POST['house_number'];?>" disabled>
      </div>

      <div class="form-content__subtitle">建物名</div>
      <div class="form-content__input">
        <input type="text" name="w" size="60" value="<?php echo $_POST['building_name'];?>" disabled>
      </div>

      <input type="hidden" name="parent_id" value="<?php echo $_POST['parent_id']; ?>">
      <input type="hidden" name="password" value="<?php echo $_POST['password']; ?>">
      <input type="hidden" name="postal_code" value="<?php echo $_POST['zip01']; ?>">
      <input type="hidden" name="address" value="<?php echo $_POST['pref01']; echo $_POST['addr01']; echo $_POST['q']; echo $_POST['w'];?>">
      <input type="hidden" name="parent_name" value="<?php echo $_POST['c']; echo $_POST['d'];?>">
      <input type="hidden" name="mail" size="30" maxlength="50" value="<?php echo $_POST['mail'];?>">

      <div class="form-content__submit_a">
        <a href="regist_parent_1.php"><input type="button" value="登録(1/2)へ戻る"></a>
        <a href="regist_parent_2.php"><input type="button" value="登録(2/2)へ戻る"></a></div>
        <div class="form-content__submit"><input type="submit" value="次へ"></div>
      </form>

      <footer class="footer">
        <p>&copy;Cent Disco</p>
      </footer>
    </body>

    </html>
