<?php
if (!isset($_COOKIE['child_key'])) {
    $_SESSION['message'] = 'ログインしてください。';
    header('Location: login.php');
    exit;
}
session_start();
if (isset($_GET['product_id'], $_GET['quantity'])) {
    // echo $_GET['product_id'];
    // echo $_GET['quantity'];
    setcookie('products[]', $_GET['product_id'], time() + 60 * 60 * 24 * 7);
    setcookie('quantity[]', $_GET['quantity'], time() + 60 * 60 * 24 * 7);
    header('Location: cart.php');
    exit;
}elseif(isset($_COOKIE['products'], $_COOKIE['quantity'])){

}else{
    header('Location: shop.php');
    exit;
}

$dsn = 'mysql:host=localhost;dbname=hew2022_it42107;charset=utf8mb4';
$db_user = 'hew2022_it42107';
$db_pass = '';
$pdo = new PDO($dsn, $db_user, $db_pass);
$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
$pdo->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);

$p = $_COOKIE['products'];
$q = $_COOKIE['quantity'];
for ($i = 0; $i < count($p); $i++) {
    $sql = "SELECT * FROM products WHERE product_id = :product_id";
    $prepare = $pdo->prepare($sql);
    $prepare->bindValue(':product_id', $p[$i]);
    $prepare->execute();
    $cart = $prepare->fetch(PDO::FETCH_ASSOC);
    $name[$i] = $cart['product_name'];
    $price[$i] = $cart['price'];
}
$carts = 0;
for ($i = 0; $i < count($price); $i++) {
    $carts += $price[$i];
}

?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <title>Cent Disco</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="robots" content="none,noindex,nofollow">
    <link rel="stylesheet" href="css/reset.css">
    <link rel="stylesheet" href="css/style_product.css">
    <link rel="stylesheet" href="css/style.css">
</head>

<body>
    <header class="header">
        <a href="index.php">
            <img src="images/logo001.png" alt="Cent Disco" class="header_logo">
        </a>
        <nav class="gnav">
            <ul class="menu">
                <li><a href="shop.html">Shop</a></li>
                <li><a href="login.php">MyPage&Login</a></li>
                <li><a href="contact.php">Contact</a></li>
                <li>
                    <a href="cart.php">
                        <img src="images/cart.png" alt="cart" class="header_cart">
                    </a>
                </li>
            </ul>
        </nav>
    </header>

    <main class="cart-wrapper">
        <div id="" class="wrapper cart-wrapper__wrapper">
            <div class="item-info cart-info">
                <?php
                $sum = 0;
                $sum_all = 0;
                for ($i = 0; $i < count($p); $i++) : ?>
                    <dl class="cart-dl">
                        <dt>商品名</dt>
                        <dd><?php echo $name[$i]; ?></dd>
                        <dt>金額</dt>
                        <dd><?php echo $price[$i]; ?></dd>
                        <dt>個数</dt>
                        <dd><?php echo $q[$i]; ?></dd>
                        <dt>合計金額</dt>
                        <dd>
                            <?php
                            $sum = (int)$price[$i] * (int)$q[$i];
                            echo $sum;
                            $sum_all += $sum;
                            ?>
                        </dd>
                    </dl>
                <?php endfor;
                setcookie('cart', $sum_all, time() + 60 * 60 * 24 * 7)
                ?>
                <h2>全ての合計金額 <?php echo $sum_all; ?></h2>
                <a class="cart-btn" href="payment.php">購入手続きへ</a>
                <a class="cart-btns" href="shop.php">買い物を続ける</a>
                <button class="cart-btns" id="cart-clear">カートを削除</button>

            </div>
        </div>
    </main>

    <footer class="footer">
        <p>&copy;Cent Disco</p>
    </footer>

    <script>
        document.getElementById("cart-clear").onclick = function() {
            document.cookie = "products[]=; max-age=0";
            document.cookie = "quantity[]=; max-age=0";
        };
    </script>

</body>

</html>