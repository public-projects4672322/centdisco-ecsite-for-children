<?php
session_start();
$error_message = '';
if (isset($_SESSION['error'])) {
$error_message =$_SESSION['error'];
unset($_SESSION['error']);
}

if (isset($_POST['parent_id'])) {
$a = $_POST['parent_id'];
$b = $_POST['password'];
setcookie('parent_id', $a, time() + 60 * 30);
setcookie('password', $b, time() + 60 * 30);
}

if (!isset($_POST['parent_id'],$_POST['password'])) {
$_SESSION['error'] = '※の項目に空白があります';
header('Location: regist_parent_1.php');
exit;
}

if(isset($_POST['password_1'])) {
if ($_POST['password_1'] != $_POST['password']) {
$_SESSION['error'] = 'パスワードが一致していません';
header('Location: regist_parent_1.php');
exit;
}
}


if(isset($_COOKIE['parent_name'])){
  $parent_name = $_COOKIE['parent_name'];
}else{
  $parent_name = "";
}
if(isset($_COOKIE['mail'])){
  $mail = $_COOKIE['mail'];
}else{
  $mail = "";
}
if(isset($_COOKIE['zip01'])){
  $zip01 = $_COOKIE['zip01'];
}else{
  $zip01 = "";
}
if(isset($_COOKIE['pref01'])){
  $pref01 = $_COOKIE['pref01'];
}else{
  $pref01 = "";
}
if(isset($_COOKIE['addr01'])){
  $addr01 = $_COOKIE['addr01'];
}else{
  $addr01 = "";
}
if(isset($_COOKIE['house_number'])){
  $house_number = $_COOKIE['house_number'];
}else{
  $house_number = "";
}
if(isset($_COOKIE['building_name'])){
  $building_name = $_COOKIE['building_name'];
}else{
  $building_name = "";
}


?>
<!DOCTYPE html>
<html lang="jp">

<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="robot" content="none, noindex, nofollow">
  <link rel="stylesheet" href="css/reset.css">
  <link rel="stylesheet" href="css/style.css">
  <title>保護者会員登録</title>
  <script src="https://ajaxzip3.github.io/ajaxzip3.js" charset="UTF-8"></script>
  <meta name="robots" content="none,noindex,nofollow">
</head>
<body>
  <header class="header">
    <a href="index.php">
      <img src="images/logo001.png" alt="Cent Disco" class="header_logo">
    </a>
    <nav class="gnav">
      <ul class="menu">
        <li><a href="shop.php">Shop</a></li>
        <li><a href="login.php">MyPage&Login</a></li>
        <li><a href="contact.php">Contact</a></li>
        
        <li>
          <a href="cart.php">
            <img src="images/cart.png" alt="cart" class="header_cart">
          </a>
        </li>
      </ul>
    </nav>
  </header>

  <main class="main-content">
		<h2 class="body__title">SignUp-保護者会員登録</h2>
    <output name="result" style="color:red"><?php  echo $error_message;?></output><br>

    <form class="form-content"  method="post" action="regist_parent_confirm.php">
      <div class="form-content__subtitle">姓名(漢字)<font color="red">　必須</font></div>
      <div class="form-content__input">
        <div class="">
          <input type="text" name="parent_name" value=<?php echo $parent_name;?>>
        </div>
      </div>

      <div class="form-content__subtitle">メールアドレス<font color="red">　必須</font></div>
      <div class="form-content__input">
        <input type="email" name="mail" size="30" maxlength="50" value=<?php echo $mail;?>>
      </div>

      <div class="form-content__subtitle">郵便番号(ハイフンなし)<font color="red">　必須</font></div>
      <div class="form-content__input">
        <input type="text" name="zip01" size="10" maxlength="7" onKeyUp="AjaxZip3.zip2addr(this,'','pref01','addr01');" value=<?php echo $zip01;?>>
      </div>

      <div class="form-content__subtitle">都道府県<font color="red">　必須</font></div>
      <div class="form-content__input">
        <input type="text" name="pref01" size="20" value=<?php echo $pref01;?>>
      </div>

      <div class="form-content__subtitle">市区町村<font color="red">　必須</font></div>
      <div class="form-content__input">
        <input type="text" name="addr01" size="60" value=<?php echo $addr01;?>>
      </div>

      <div class="form-content__subtitle">丁目 - 番地<font color="red">　必須</font></div>
      <div class="form-content__input">
        <input type="text" name="house_number" size="60" value=<?php echo $house_number;?>>
      </div>

      <div class="form-content__subtitle">建物名</div>
      <div class="form-content__input">
        <input type="text" name="building_name" size="60" value=<?php echo $building_name;?>>
      </div>

      <div class="form-content__submit"><input type="submit" value="次へ"></div>
      </form>
      <footer class="footer">
        <p>&copy;Cent Disco</p>
      </footer>
    </body>

    </html>
