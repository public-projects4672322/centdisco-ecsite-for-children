<?php
session_start();
if (!isset($_COOKIE['parent_key'])) {
  $_SESSION['message'] = 'ログインしてください。';
  header('Location: login.php');
  exit;
}
if (isset($_POST['money_parent'])) {
  $money_parent = (int)$_POST['money_parent'];
  $flg = 1;
  if($money_parent <= 0){
    $_SESSION['message'] = '1円以上の金額を入力してください';
    header('Location: money_parent.php');
    exit;
  }
} elseif (isset($_POST['money_child'], $_POST['child_id'])) {
  $money_child = (int)$_POST['money_child'];
  $child_id = $_POST['child_id'];
  $flg = 2;
  if ($money_child <= 0) {
    $_SESSION['message1'] = '1円以上の金額を入力してください';
    header('Location: money_parent.php');
    exit;
  }
} else {
  header('Location: money_parent.php');
}



$dsn = 'mysql:host=localhost;dbname=hew2022_it42107;charset=utf8mb4';
$db_user = 'hew2022_it42107';
$db_pass = '';

try {
  $pdo = new PDO($dsn, $db_user, $db_pass);
  $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
  $pdo->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);

  $sql = "SELECT * FROM parentbalance WHERE parent_id = :parent_id";
  $qry = $pdo->prepare($sql);
  $qry->bindValue(':parent_id', $_COOKIE['parent_key']);
  $qry->execute();
  $parent_id = $qry->fetch();

  if ($flg == 1) {
    // 残高計算
    $parent_balance = $parent_id['parent_balance'] + $money_parent;

    $sql = 'UPDATE parentbalance SET parent_balance = :parent_balance WHERE parent_id = :parent_id';
    $qry = $pdo->prepare($sql);
    $qry->bindValue(':parent_balance', $parent_balance);
    $qry->bindValue(':parent_id', $_COOKIE['parent_key']);
    $qry->execute();
    header('Location: money_parent.php');
  } elseif ($flg == 2) {
    // 残高計算
    $parent_balance = $parent_id['parent_balance'] - $money_child;
    if($parent_balance < 0){
      $_SESSION['message'] = '金額が足りません。チャージするか金額を変更してください。';
      header('Location: money_parent.php');
      exit;
    }
    // 親残高更新
    $sql = 'UPDATE parentbalance SET parent_balance = :parent_balance WHERE parent_id = :parent_id';
    $qry = $pdo->prepare($sql);
    $qry->bindValue(':parent_balance', $parent_balance);
    $qry->bindValue(':parent_id', $_COOKIE['parent_key']);
    $qry->execute();

    // 子残高セレクト
    $sql = "SELECT * FROM childbalance WHERE child_id = :child_id";
    $qry = $pdo->prepare($sql);
    $qry->bindValue(':child_id', $child_id);
    $qry->execute();
    $child_info = $qry->fetch();

    // 子残高計算
    $child_balance = $child_info['child_balance'] + $money_child;
    // 子残高更新
    $sql = 'UPDATE childbalance SET child_balance = :child_balance WHERE child_id = :child_id';
    $qry = $pdo->prepare($sql);
    $qry->bindValue(':child_balance', $child_balance);
    $qry->bindValue(':child_id', $child_id);
    $qry->execute();
    header('Location: money_parent.php');

  }
} catch (PDOException $e) {
  echo 'DB接続エラー : ' . $e->getMessage();
}