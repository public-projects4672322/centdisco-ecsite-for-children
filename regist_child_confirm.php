<?php


session_start();
if ($_POST['child_name'] == ''){
  $_SESSION['error'] = '※の項目に空白があります';
  header('Location: regist_child_2.php');
  exit;
}else {
  setcookie('child_name', $_POST['child_name'], time() + 60 *30);
  if(!$_POST['username'] == ''){
    setcookie('username', $_POST['username'], time() + 60 * 30);
  }
}
?>
<!DOCTYPE html>
<html lang="jp">

<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="robot" content="none, noindex, nofollow">
  <link rel="stylesheet" href="css/reset.css">
  <link rel="stylesheet" href="css/style.css">
  <title>利用者会員登録確認画面</title>
  <script src="https://ajaxzip3.github.io/ajaxzip3.js" charset="UTF-8"></script>
  <meta name="robots" content="none,noindex,nofollow">
</head>
<body>
  <header class="header">
    <a href="index.php">
      <img src="images/logo001.png" alt="Cent Disco" class="header_logo">
    </a>
    <nav class="gnav">
      <ul class="menu">
        <li><a href="shop.php">Shop</a></li>
        <li><a href="login.php">MyPage&Login</a></li>
        <li><a href="contact.php">Contact</a></li>
        
        <li>
          <a href="cart.php">
            <img src="images/cart.png" alt="cart" class="header_cart">
          </a>
        </li>
      </ul>
    </nav>
  </header>

  <main class="main-content">
    <h2 class="body__title">SignUp-利用者会員登録確認画面</h2>
    <form class="form-content" method="post" action="regist_child_confirm_2.php?key=data&a=1">

      <div class="form-content__subtitle">氏名<font color="red">　必須</font></div>
      <div class="form-content__input">
        <input type="text" name="a" value="<?php echo $_POST['child_name'];?>" disabled>
      </div>

      <div class="form-content__subtitle">ユーザネーム</div>
      <div class="form-content__input">
        <input type="text" name="a" value="<?php echo $_POST['username'];?>" disabled>
      </div>

      <div class="form-content__submit_a">
        <a href="regist_child_1.php"><input type="button" value="登録(1/2)へ戻る"></a>
        <a href="regist_child_2.php"><input type="button" value="登録(2/2)へ戻る"></a></div>
        <div class="form-content__submit"><input type="submit" value="次へ"></div>
      </form>
      <footer class="footer">
        <p>&copy;Cent Disco</p>
      </footer>
    </body>

    </html>
