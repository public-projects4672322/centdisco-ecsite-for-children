<?php
session_start();
if (!isset($_COOKIE['child_key'])) {
  $_SESSION['message'] = 'ログインしてください。';
header('Location: login.php');
  exit;
}
echo $_POST['count1'];
echo $_POST['count2'];
echo $_POST['count3'];
echo $_POST['count4'];
echo $_POST['count5'];
echo $_POST['count6'];
echo $_POST['count7'];
echo $_POST['count8'];
echo $_POST['count9'];

$change = $_POST['cent'];
if ($change == -1) {
    $_SESSION['message'] = '購入金額以上のお金をトレーに入れてください。';
    header('Location: payment.php');
    exit;
} else if (!isset($change)) {
    $_SESSION['message'] = 'トレーにお金を入れてください。';
    header('Location: payment.php');
    exit;
}
$id = $_COOKIE['child_key'];
$dsn = 'mysql:host=localhost;dbname=hew2022_it42107;charset=utf8mb4';
$db_user = 'hew2022_it42107';
$db_pass = '';
$pdo = new PDO($dsn, $db_user, $db_pass);
$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
$pdo->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
$sql1 = "SELECT * FROM child_moneycount WHERE child_id = :id";
$prepare1 = $pdo->prepare($sql1);
$prepare1->bindValue(':id', $id);
$prepare1->execute();
$money = $prepare1->fetch(PDO::FETCH_ASSOC);

$cnt1 = $money['ten_thousand'];
$cnt2 = $money['five_thousand'];
$cnt3 = $money['one_thousand'];
$cnt4 = $money['five_hundred'];
$cnt5 = $money['one_hundred'];
$cnt6 = $money['fifty'];
$cnt7 = $money['ten'];
$cnt8 = $money['five'];
$cnt9 = $money['one'];

$cnt1 -= $_POST['count1'];
$cnt2 -= $_POST['count2'];
$cnt3 -= $_POST['count3'];
$cnt4 -= $_POST['count4'];
$cnt5 -= $_POST['count5'];
$cnt6 -= $_POST['count6'];
$cnt7 -= $_POST['count7'];
$cnt8 -= $_POST['count8'];
$cnt9 -= $_POST['count9'];

// おつり紙幣硬貨配列
$change_money = [5000, 1000, 500, 100, 50, 10, 5, 1];
// おつり何枚
$how_many = [];
foreach($change_money as $change_count){
    $how_many[$change_count] = floor($change / $change_count);
    $change %= $change_count;
    if($change == 0){
      break;
    }
}
//おつり加算
if (isset($how_many[5000])) {
    $cnt2 += $how_many[5000];
}
if (isset($how_many[1000])) {
    $cnt3 += $how_many[1000];
    echo $cnt3;
}
if (isset($how_many[500])) {
    $cnt4 += $how_many[500];
}
if (isset($how_many[100])) {
    $cnt5 += $how_many[100];
}
if (isset($how_many[50])) {
    $cnt6 += $how_many[50];
}
if (isset($how_many[10])) {
    $cnt7 += $how_many[10];
}
if (isset($how_many[5])) {
    $cnt8 += $how_many[5];
}
if (isset($how_many[1])) {
    $cnt9 += $how_many[1];
}
$sql2 = "UPDATE child_moneycount SET ten_thousand = :cnt1, five_thousand = :cnt2, one_thousand = :cnt3, five_hundred = :cnt4, one_hundred = :cnt5, fifty = :cnt6, ten = :cnt7, five = :cnt8, one = :cnt9 WHERE child_id = :id";
$prepare2 = $pdo->prepare($sql2);
$prepare2->bindValue(':id', $id);
$prepare2->bindValue(':cnt1', $cnt1);
$prepare2->bindValue(':cnt2', $cnt2);
$prepare2->bindValue(':cnt3', $cnt3);
$prepare2->bindValue(':cnt4', $cnt4);
$prepare2->bindValue(':cnt5', $cnt5);
$prepare2->bindValue(':cnt6', $cnt6);
$prepare2->bindValue(':cnt7', $cnt7);
$prepare2->bindValue(':cnt8', $cnt8);
$prepare2->bindValue(':cnt9', $cnt9);
$prepare2->execute();

$sql = "SELECT * FROM childbalance WHERE child_id = :id";
$qry = $pdo->prepare($sql);
$qry->bindValue(':id', $id);
$qry->execute();
$temp = $qry->fetch();

$child_balance = $temp['child_balance'] - (int)$_COOKIE['cart'];

$sql3 = "UPDATE childbalance SET child_balance = :child_balance WHERE child_id = :id";
$prepare3 = $pdo->prepare($sql3);
$prepare3->bindValue(':id', $id);
$prepare3->bindValue(':child_balance', $child_balance);
$prepare3->execute();




header('Location: payment_complete.php');
exit;