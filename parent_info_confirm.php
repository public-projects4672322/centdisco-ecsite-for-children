<?php
if (!isset($_COOKIE['parent_key'])) {
  $_SESSION['message'] = '保護者用アカウントでログインしてください。';
  header('Location: login.php');
  exit;
}
if(!isset($_POST['parent_name'], $_POST['postal_code'],$_POST['address'],$_POST['mail'])){
  header('Location: parent_info.php');
}

$dsn = 'mysql:host=localhost;dbname=hew2022_it42107;charset=utf8mb4';
$db_user = 'hew2022_it42107';
$db_pass = '';
$parent_id = $_COOKIE['parent_key'];
try {
  $pdo = new PDO($dsn, $db_user, $db_pass);
  $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
  $pdo->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);

  $sql = "UPDATE `parents` SET `password`=:password, `postal_code`=:postal_code, `address`=:address, `parent_name`=:parent_name, `mail`=:mail WHERE `parent_id` = :parent_id";
  $qry = $pdo->prepare($sql);
  $qry->bindValue(':password', $_POST['password']);
  $qry->bindValue(':postal_code', $_POST['postal_code']);
  $qry->bindValue(':address', $_POST['address']);
  $qry->bindValue(':parent_name', $_POST['parent_name']);
  $qry->bindValue(':mail', $_POST['mail']);
  $qry->bindValue(':parent_id', $_COOKIE['parent_key']);
  $qry->execute();

  header('Location: parent_info.php');
  exit;

} catch (PDOException $e) {
  echo $e->getMessage();
  header('Location: mypage_parent.php');
  exit;
}

?>
