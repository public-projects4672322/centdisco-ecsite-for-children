<?php
session_start();
if (!isset($_COOKIE['child_key'])) {
  $_SESSION['message'] = 'ログインしてください。';
	header('Location: login.php');
  exit;
}
if(!isset($_POST['child_change'])){
  $_SESSION['message'] = '下記の内容を入力・選択してください。';
  header('Location: child_change.php');
  exit;
}

if(!$_POST['username'] == ""){
  $username = $_POST['username'];
}else{
  $username = "";
}

if($_POST['icon'] == ""){
  $_SESSION['message'] = '下記の内容を入力・選択してください。';
  header('Location: child_change.php');
}


$dsn = 'mysql:host=localhost;dbname=hew2022_it42107;charset=utf8mb4';
$db_user = 'hew2022_it42107';
$db_pass = '';

try{
  $pdo = new PDO($dsn, $db_user, $db_pass);
  $pdo->setAttribute(PDO::ATTR_ERRMODE,PDO::ERRMODE_EXCEPTION);
  $pdo->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);

  if(!$username == ""){
    $sql = "UPDATE children SET username = :username WHERE child_id = :child_id";
    $qry = $pdo->prepare($sql);
    $qry->bindValue(':username', $username);
    $qry->bindValue(':child_id', $_COOKIE['child_key']);
    $qry->execute();
  }
  $sql = "UPDATE level SET icon_path = :icon_path WHERE child_id = :child_id";
  $qry = $pdo->prepare($sql);
  $qry->bindValue(':icon_path', $_POST['icon']);
  $qry->bindValue(':child_id', $_COOKIE['child_key']);
  $qry->execute();

} catch (PDOException $e) {
  echo 'DB接続エラー ： ' . $e->getMessage();
}

header('Location: mypage_child.php');