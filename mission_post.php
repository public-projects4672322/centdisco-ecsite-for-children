<?php
session_start();

//ログイン判定
// setcookie(
//     "parent_key", //キー名称
//     "prnt001", //データ
//     time() + 60 * 60 * 2
// );
if( !isset($_COOKIE['parent_key']) ) {
    //ログイン成功時のみcookieに保存される仕組みなので、dbのカラムの有無の判定は省略
    header('Location: index.php');
    exit;
}

//テーブル存在チェック関数
function table_exists($pdo, $table) {
    $result = $pdo -> query("SHOW TABLES LIKE '{$table}'");

    if( $result -> rowCount() == 1 ) {
        return true;
    } else {
        return false;
    }
}

//カラム存在チェック関数
function column_exists($pdo, $table) {
    $result = $pdo -> query("SELECT * FROM $table /* WHERE deleted_at IS NULL */");

    if( $result -> rowCount() > 0 ) {
        return true;
    } else {
        return false;
    }
}

//ミッション存在チェック関数
function mission_exists($pdo, $table, $id) {
    $result = $pdo -> query("SELECT * FROM $table WHERE mission_id = $id /* AND deleted_at IS NULL */");

    if( $result -> rowCount() == 1 ) {
        return true;
    } else {
        return false;
    }
}

// DB登録 //////////////////////////////////////////////////////////////////////////////////////
try {
    //DSN(Data Source Name)…DBへの接続情報
    $dsn = 'mysql:host=localhost;dbname=hew2022_it42107;charset=utf8mb4';
    $db_user = 'hew2022_it42107';
    $db_pass = '';

    //DB接続(open) イメージコマンドのmysql -u root -p xxx
    $pdo = new PDO($dsn, $db_user, $db_pass);

    //デフォルトの動作はエラー黙殺
    //→エラー時、そのアラートを発生するようにする
    $pdo -> setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    //DB存在チェック(パーミッションがあること前提)
    $pdo -> query("CREATE DATABASE IF NOT EXISTS hew2022_it42107");

    $dsn = 'mysql:host=localhost;dbname=hew2022_it42107;charset=utf8mb4';
    $pdo = new PDO($dsn, $db_user, $db_pass);
} catch(PDOException $e) {
    //簡易エラー処理
    echo $e -> getMessage();

    //本来はログを出力し、エラーページなどに飛ばして終了の形が多い
    exit;
}

if( isset($_GET['mission_id']) ) {
    //GETで受け取ったidのミッションと、そのミッションのparent_idがログイン中の親のアカウントidと一致するかチェック
    //親アカウントが一致すればok
    $result = $pdo -> query("SELECT * FROM mission WHERE mission_id = {$_GET['mission_id']} AND parent_id = '{$_COOKIE['parent_key']}'");

    if( $result -> rowCount() != 1 ) {
        //一致しないとき
        //エラーメッセージ
        $_SESSION['failed'] = 'アカウントが一致しません。指定されたidのミッションの内容は表示されません。<br>';

        //GET無しのリンクへ
        header('Location: mission_post.php');
        exit;
    }
}

$startDate = date('Y-m-d');
?>

<!DOCTYPE html>
<html lang="ja">
    <head>
        <title>ミッションの管理</title>
        <meta charset="utf-8">
				<link rel="stylesheet" href="css/style.css">
				<link rel="stylesheet" href="css/reset.css">
        <script type="text/javascript">
            //ブラウザバック時などにフォームをリセット
            window.onpageshow = () => {
                document.forms.missionForm.reset()
            }

            //回数指定のチェックボックス制御
            function enableCount() {
                //count：回数入力フォーム名
                //times：回数指定のチェックボックスのフォーム名

                //回数指定のチェック時、回数指定入力フォーム有効化
                document.forms.missionForm.count.disabled = !document.forms.missionForm.times.checked

                //回数指定のチェック時、永続指定のチェックボックス無効化
                document.forms.missionForm.always.disabled = document.forms.missionForm.times.checked

                //回数指定チェックを外した時に回数入力フォームをリセット
                if(!document.forms.missionForm.times.checked) {
                    document.forms.missionForm.count.value = null
                }
            }

            //永続指定のチェックボックス制御
            function alwaysCount() {
                //永続指定チェック時、回数指定チェックボックス無効化
                document.forms.missionForm.times.disabled = document.forms.missionForm.always.checked
                //永続指定チェック時、回数指定入力フォーム無効化
                document.forms.missionForm.count.disabled = document.forms.missionForm.always.checked

                //回数指定チェック外し
                document.forms.missionForm.times.checked = false
                //回数入力フォームをリセット
                document.forms.missionForm.count.value = null
            }

            //開始日と期間に応じて終了日を自動入力
            function startDate() {
                //開始日
                var startDate = new Date(document.forms.missionForm.start.value)
                //終了日
                var endDate = startDate

                console.log('startDate(),startDate:' + startDate)

                if(startDate != 'Invalid Date' && endDate != 'Invalid Date') {
                    //日付が不正でないとき
                    if( document.forms.missionForm.period_number.value.match(/^[0-9]+$/) ) {
                        //選択フォームの内容に応じて
                        if(document.forms.missionForm.interval.value == 'days') {
                            endDate = endDate.setDate( startDate.getDate() + parseInt(document.forms.missionForm.period_number.value) )
                        } else if(document.forms.missionForm.interval.value == 'weeks') {
                            endDate = endDate.setDate( startDate.getDate() + parseInt(document.forms.missionForm.period_number.value) * 7)
                        } else if(document.forms.missionForm.interval.value == 'months') {
                            endDate = endDate.setMonth( startDate.getMonth() + parseInt(document.forms.missionForm.period_number.value) )
                        } else if(document.forms.missionForm.interval.value == 'year') {
                            endDate = endDate.setFullYear( startDate.getFullYear() + parseInt(document.forms.missionForm.period_number.value) )
                        }
                    }

                    //書式修正
                    endDate = new Date(endDate)
                    console.log('startDate(),endDate:' + endDate)

                    //書式修正(yyyy-mm-dd)
                    var y = ('0000' + endDate.getFullYear()).slice(-4)
                    var m = ('00' + (endDate.getMonth()+1)).slice(-2)
                    var d = ('00' + endDate.getDate()).slice(-2)
                    console.log('startDate(),end:' + y + '-' + m + '-' + d)
                    document.forms.missionForm.end.value = y + '-' + m + '-' + d
                } else {
                    //日付が不正

                    startDate = new Date()
                    //書式修正(yyyy-mm-dd)
                    var y = ('0000' + startDate.getFullYear()).slice(-4)
                    var m = ('00' + (startDate.getMonth()+1)).slice(-2)
                    var d = ('00' + startDate.getDate()).slice(-2)
                    document.forms.missionForm.start.value = y + '-' + m + '-' + d

                    endDate = new Date()
                    //書式修正(yyyy-mm-dd)
                    var y = ('0000' + endDate.getFullYear()).slice(-4)
                    var m = ('00' + (endDate.getMonth()+1)).slice(-2)
                    var d = ('00' + endDate.getDate()).slice(-2)
                    document.forms.missionForm.end.value = y + '-' + m + '-' + d
                }
            }

            //終了日に応じて期間を自動入力
            function endDate() {
                //開始日
                var startDate = new Date(document.forms.missionForm.start.value)
                //終了日
                var endDate = new Date(document.forms.missionForm.end.value)

                console.log('endDate(),startDate:' + startDate)
                console.log('endDate(),endDate:' + endDate)

                if(startDate != 'Invalid Date' && endDate != 'Invalid Date') {
                    //日付が不正でないとき

                    //差日（86,400,000ミリ秒＝１日）
                    var interval = (endDate - startDate) / 86400000
                    console.log('endDate(),interval:' + interval)

                    //日数自動入力
                    document.forms.missionForm.period_number.value = interval
                    if(interval <= 0) {
                        //開始日 > 終了日のとき、マイナスの値にならないようにする
                        document.forms.missionForm.period_number.value = 0
                    }
                    
                    //期間(日/週間/ヶ月/年)自動選択
                    document.forms.missionForm.interval.value = 'days'
                } else {
                    //日付が不正

                    startDate = new Date()
                    //書式修正(yyyy-mm-dd)
                    var y = ('0000' + startDate.getFullYear()).slice(-4)
                    var m = ('00' + (startDate.getMonth()+1)).slice(-2)
                    var d = ('00' + startDate.getDate()).slice(-2)
                    document.forms.missionForm.start.value = y + '-' + m + '-' + d

                    endDate = new Date()
                    //書式修正(yyyy-mm-dd)
                    var y = ('0000' + endDate.getFullYear()).slice(-4)
                    var m = ('00' + (endDate.getMonth()+1)).slice(-2)
                    var d = ('00' + endDate.getDate()).slice(-2)
                    document.forms.missionForm.end.value = y + '-' + m + '-' + d
                }
            }

            //開始日 > 終了日にならないようにする
            function minDate() {
                //開始日
                var startDate = new Date(document.forms.missionForm.start.value)
                //終了日
                var endDate = new Date(document.forms.missionForm.end.value)

                if(startDate != 'Invalid Date' && endDate != 'Invalid Date') {
                    //日付が不正でないとき
                    if(startDate > endDate) {
                        //書式修正(yyyy-mm-dd)
                        var y = ('0000' + endDate.getFullYear()).slice(-4)
                        var m = ('00' + (startDate.getMonth()+1)).slice(-2)
                        var d = ('00' + startDate.getDate()).slice(-2)
                        console.log('minDate(),minDate:' + y + '-' + m + '-' + d)
                        document.forms.missionForm.end.value = y + '-' + m + '-' + d

                        console.log('minDate(),startDate:' + startDate)
                        console.log('minDate(),endDate:' + endDate)
                    }
                } else {
                    //日付が不正

                    startDate = new Date()
                    //書式修正(yyyy-mm-dd)
                    var y = ('0000' + startDate.getFullYear()).slice(-4)
                    var m = ('00' + (startDate.getMonth()+1)).slice(-2)
                    var d = ('00' + startDate.getDate()).slice(-2)
                    document.forms.missionForm.start.value = y + '-' + m + '-' + d

                    endDate = new Date()
                    //書式修正(yyyy-mm-dd)
                    var y = ('0000' + endDate.getFullYear()).slice(-4)
                    var m = ('00' + (endDate.getMonth()+1)).slice(-2)
                    var d = ('00' + endDate.getDate()).slice(-2)
                    document.forms.missionForm.end.value = y + '-' + m + '-' + d
                }
            }

            //回数指定時、最終的な終了日時の算出
            function finalEndDate() {
                document.getElementById('finalEndDate').style.display = "none"
                // console.log(document.forms.missionForm.count.value)

                if( document.forms.missionForm.period_number.value.match(/^[0-9]+$/) && document.forms.missionForm.count.value.match(/^[0-9]+$/) ) {
                    //回数入力時のみ出力
                    //開始日
                    var startDate = new Date(document.forms.missionForm.start.value)
                    //終了日
                    var endDate = new Date(document.forms.missionForm.end.value)

                    if(startDate != 'Invalid Date' && endDate != 'Invalid Date') {
                    //日付が不正でないとき

                        //最終的な終了日
                        var finalEndDate = startDate

                        //期間
                        //選択フォームの内容に応じて
                        if(document.forms.missionForm.interval.value == 'days') {
                            finalEndDate = finalEndDate.setDate( startDate.getDate() + parseInt(document.forms.missionForm.period_number.value) * parseInt(document.forms.missionForm.count.value) )
                        } else if(document.forms.missionForm.interval.value == 'weeks') {
                            finalEndDate = finalEndDate.setDate( startDate.getDate() + parseInt(document.forms.missionForm.period_number.value) * 7 * parseInt(document.forms.missionForm.count.value) )
                        } else if(document.forms.missionForm.interval.value == 'months') {
                            finalEndDate = finalEndDate.setMonth( startDate.getMonth() + parseInt(document.forms.missionForm.period_number.value) * parseInt(document.forms.missionForm.count.value) )
                        } else if(document.forms.missionForm.interval.value == 'year') {
                            finalEndDate = finalEndDate.setFullYear( startDate.getFullYear() + parseInt(document.forms.missionForm.period_number.value) * parseInt(document.forms.missionForm.count.value) )
                        }

                        //書式修正
                        finalEndDate = new Date(finalEndDate)
                        console.log('finalEndDate(),finalEndDate:' + finalEndDate)

                        //書式修正、出力
                        var y = finalEndDate.getFullYear()
                        if( finalEndDate.getFullYear() < 10000 ) {
                            y = ('0000' + finalEndDate.getFullYear()).slice(-4)
                        }
                        var m = ('00' + (finalEndDate.getMonth()+1)).slice(-2)
                        var d = ('00' + finalEndDate.getDate()).slice(-2)
                        const weekday = ['(日)', '(月)', '(火)', '(水)', '(木)', '(金)', '(土)']
                        var w = weekday[finalEndDate.getDay()]
                        console.log('finalEndDate(),finalEndDate:' + y + '-' + m + '-' + d + w)

                        document.getElementById('finalEndDate').style.display = "block"
                        document.getElementById('finalEndDate').innerHTML = 'このミッションを' + document.forms.missionForm.count.value + '回こなした時の終了日：' + y + '年' + m + '月' + d + '日' + w
                    } else {
                        //日付が不正

                        startDate = new Date()
                        //書式修正(yyyy-mm-dd)
                        var y = ('0000' + startDate.getFullYear()).slice(-4)
                        var m = ('00' + (startDate.getMonth()+1)).slice(-2)
                        var d = ('00' + startDate.getDate()).slice(-2)
                        document.forms.missionForm.start.value = y + '-' + m + '-' + d

                        endDate = new Date()
                        //書式修正(yyyy-mm-dd)
                        var y = ('0000' + endDate.getFullYear()).slice(-4)
                        var m = ('00' + (endDate.getMonth()+1)).slice(-2)
                        var d = ('00' + endDate.getDate()).slice(-2)
                        document.forms.missionForm.end.value = y + '-' + m + '-' + d
                    }
                }
            }
        </script>
    </head>
    <body>
		<header class="header">
    <a href="index.php">
      <img src="images/logo001.png" alt="Cent Disco" class="header_logo">
    </a>
    <nav class="gnav">
    	<ul class="menu">
        <li><a href="shop.php">Shop</a></li>
  	    <li><a href="login.php">MyPage&Login</a></li>
	      <li><a href="contact.php">Contact</a></li>
      	<li>
         	<a href="cart.php">
          	<img src="images/cart.png" alt="cart" class="header_cart">
         	</a>
       	</li>
     	</ul>
    </nav>
  	</header>
        <main class="main-content">
        <h1 class="body__title">ミッションの管理</h1>

        <?php
        //mission_add.phpから受け取った成功メッセージ出力
        if( isset($_SESSION['success']) ) { ?>
            <font color="blue"><?php echo $_SESSION['success']; ?></font>
        <?php } ?>

        <?php
        //mission_add.phpから受け取ったエラーメッセージ出力
        if( isset($_SESSION['error']) ) { ?>
            <font color="red"><?php echo $_SESSION['error']; ?></font>
        <?php } ?>

        <form id="missionForm" action="mission_add.php" method="post" class="form-content">
            <dt class="form-content__subtitle">01 ユーザー選択</dt>
						<dd class="form-content__input">
            <p style="margin-bottom:10px;">ミッションを追加したいお子様のアカウントを選択してください。</p>
            <!-- 子供のユーザー情報は存在すること前提 -->
            <select name="child" required>
                <?php
                //データ取得
                // $sql = "SELECT * FROM children WHERE deleted_at IS NULL";
                $sql = "SELECT * FROM children WHERE parent_id = '{$_COOKIE['parent_key']}'";
                // $sql = "SELECT * FROM children";
                $stmt = $pdo -> prepare($sql);
                $stmt -> execute();

                $i = 0;
                while( $child = $stmt -> fetch(PDO::FETCH_ASSOC) ) {
                    $children[] = $child;

                    // foreach($children as $key => $value) {
                    //     echo $key. '=>'. $value. '、';
                    // }
                    // echo "<br>";
                    ?>

                    <!-- 出力 -->
                    <option value="<?php echo htmlspecialchars($children[$i]['child_id']); ?>" <?php
                    $m = '';
                    if( isset($_GET['mission_id']) && mission_exists($pdo, 'mission', $_GET['mission_id']) ) {
                        //GET有りの時
                        //初期の選択肢を設定
                        $sql2 = "SELECT * FROM mission as m INNER JOIN children as c ON m.child_id = c.child_id and m.parent_id = c.parent_id WHERE mission_id = {$_GET['mission_id']}";
                        $stmt2 = $pdo -> prepare($sql2);
                        $stmt2 -> execute();

                        $mission = $stmt2 -> fetch(PDO::FETCH_ASSOC);

                        if($children[$i]['child_id'] == $mission['child_id']) {
                            $m = 'selected';
                        }
                    } else if( isset($_SESSION['child_id']) && $_SESSION['child_id'] == $children[$i]['child_id'] ) {
                        //前回選択したユーザーを初期値にする
                        $m = 'selected';
                    } else if(isset($_POST['child']) && $_POST['child'] == $children[$i]['child_id']) {
                        $m = 'selected';
                    }

                    echo $m;
                    ?>><?php echo htmlspecialchars($children[$i]['username']); ?>様 - ID：<?php echo htmlspecialchars($children[$i]['child_id']); ?> - お子様用アカウント</option>

                    <?php $i++;
                }
                // print_r($products);
                ?>

                <!-- <input type="submit" value="確定"> -->
            </select>
						</dd>
            <dt class="form-content__subtitle">02 ミッション追加</dt>
						<dd class="form-content__input mission-post">
            <p><font color="red">＊</font>の印が付いた項目は必須入力です。</p>
            <?php
            //mission_status.phpから受け取ったGETパラメータ
            if( isset($_GET['mission_id']) && mission_exists($pdo, 'mission', $_GET['mission_id']) ) {
                echo '<p><font color="blue">変更した内容を変更前の状態に戻すには、このページをリロードしてください。<br>「ミッション更新」ボタンを押下する前であれば、入力状態が変更前の内容に戻ります。</font></p>';
            } else if( isset($_GET['mission_id']) ) {
                echo '<p><font color="red">不正なGETパラメータです。このミッションは新規作成されます。</font></p>';
            }
            ?>

            <?php
            //このページから受け取ったエラーメッセージ出力
            if( isset($_SESSION['failed']) ) { ?>
                <font color="blue"><?php echo $_SESSION['failed']; ?></font>
            <?php } ?>

            ミッション名<font color="red">＊</font><br>
            <input type="text" name="name" value="<?php
            //mission_status.phpから受け取ったGETパラメータ
            if( isset($_GET['mission_id']) && mission_exists($pdo, 'mission', $_GET['mission_id']) ) {
                //GET有り
                echo htmlspecialchars($mission['mission_name']);
            } else if(isset($_POST['name'])) {
                echo htmlspecialchars($_POST['name']);
            }
            ?>" required><br>

            ミッション内容詳細<br>
            <textarea name="detail"><?php
            //mission_status.phpから受け取ったGETパラメータ
            if( isset($_GET['mission_id']) && mission_exists($pdo, 'mission', $_GET['mission_id']) ) {
                //GET有り
                echo htmlspecialchars($mission['mission_detail']);
            } else if(isset($_POST['detail'])) {
                echo htmlspecialchars($_POST['detail']);
            }
            ?></textarea><br>

            <p>期間<font color="red">＊</font></p><br>
            開始日<br>
            <input type="date" name="start" value="<?php
            //mission_status.phpから受け取ったGETパラメータ
            if( isset($_GET['mission_id']) && mission_exists($pdo, 'mission', $_GET['mission_id']) ) {
                $start_date = ( new DateTime($mission['start_date']) ) -> format('Y-m-d');
                echo $start_date;
            } else if(isset($_POST['start'])) {
                echo htmlspecialchars($_POST['start']);
            } else {
                echo date('Y-m-d');
            }
            ?>" onkeyup="startDate(), endDate(), finalEndDate()" onchange="startDate(), endDate(), finalEndDate()" min="0001-01-01" max="9999-12-31" required><br>

            期間を指定<br>
            <input type="text" pattern="^[0-9]*$" name="period_number" value="<?php
            //mission_status.phpから受け取ったGETパラメータ
            if( isset($_GET['mission_id']) && mission_exists($pdo, 'mission', $_GET['mission_id']) ) {
                //GET有り
                echo htmlspecialchars($mission['date_interval']);
            } else if(isset($_POST['period_number'])) {
                echo htmlspecialchars($_POST['period_number']);
            }
            ?>" onkeyup="startDate(), finalEndDate()" onchange="startDate(), finalEndDate()" required style="width:50px;">

            <select name="interval" onkeyup="startDate(), finalEndDate()" onchange="startDate(), finalEndDate()" required  style="width:70px;">
                <option value="days" onchange="startDate(), finalEndDate()" <?php
                //mission_status.phpから受け取ったGETパラメータ
                if( isset($_GET['mission_id']) && mission_exists($pdo, 'mission', $_GET['mission_id']) ) {
                    //GET有り
                    echo 'selected';
                } else if(isset($_POST['interval']) && $_POST['interval'] == 'days') {
                    echo 'selected';
                }
                ?>>日</option>
                <option value="weeks" onchange="startDate(), finalEndDate()" <?php
                if(isset($_POST['interval']) && $_POST['interval'] == 'weeks') {
                    echo 'selected';
                }
                ?>>週間</option>
                <option value="months" onchange="startDate(), finalEndDate()" <?php
                if(isset($_POST['interval']) && $_POST['interval'] == 'months') {
                    echo 'selected';
                }
                ?>>ヶ月</option>
                <option value="year" onchange="startDate(), finalEndDate()" <?php
                if(isset($_POST['interval']) && $_POST['interval'] == 'year') {
                    echo 'selected';
                }
                ?>>年</option>
            </select>後に終了<br>

            <label><input type="checkbox" name="times" value="true" onclick="enableCount(), finalEndDate()" <?php
            //mission_status.phpから受け取ったGETパラメータ
            $flg = false;
            if( isset($_GET['mission_id']) && mission_exists($pdo, 'mission', $_GET['mission_id']) ) {
                //GET有り
                if($mission['count'] != null) {
                    echo 'checked';

                    //回数指定フォーム有効化のため
                    $flg = true;
                }

                if($mission['permanent'] == 'yes') {
                    echo 'disabled="disabled"';
                }
            } else if(isset($_POST['times'])) {
                echo 'checked';

                //回数指定フォーム有効化のため
                $flg = true;
            } else if(isset($_POST['always'])) {
                echo 'disabled';
            }
            ?>>回数を指定する場合は、ここをチェックしてください</label><br>

            <input style="margin-bottom:10px;" type="text" pattern="^[1-9][0-9]*$" name="count" <?php if(!$flg || isset($_GET['mission_id']) && $mission['default_count'] == null) { echo 'disabled="disabled"'; }?> value="<?php
            //mission_status.phpから受け取ったGETパラメータ
            if( isset($_GET['mission_id']) && mission_exists($pdo, 'mission', $_GET['mission_id']) && $mission['default_count'] != null ) {
                //GET有り
                echo htmlspecialchars($mission['default_count']);
            } else if(isset($_POST['times'])) {
                echo htmlspecialchars($_POST['count']);
            }
            ?>" onkeyup="finalEndDate()" onchange="finalEndDate()" required>回<br>

            <label><input type="checkbox" name="always" value="true" onclick="alwaysCount(), enableCount(), finalEndDate()" <?php
            //mission_status.phpから受け取ったGETパラメータ
            if( isset($_GET['mission_id']) && mission_exists($pdo, 'mission', $_GET['mission_id']) ) {
                //GET有り
                if($mission['permanent'] == 'yes') {
                    echo 'checked';
                } else if($flg) {
                    echo 'disabled="disabled"';
                }
            } else if(isset($_POST['always'])) {
                echo 'checked';
            } else if($flg) {
                echo 'disabled="disabled"';
            }
            ?>>回数を指定せずに常時出題したい場合は、ここをチェックしてください</label><br>

            <p  style="margin-top:90px;">ミッションの初回終了日</p><br>
            <input type="date" name="end" value="<?php
            //mission_status.phpから受け取ったGETパラメータ
            if( isset($_GET['mission_id']) && mission_exists($pdo, 'mission', $_GET['mission_id']) ) {
                //GET有り
                //終了日
                $end_date = ( new DateTime($mission['start_date']) ) -> modify("+". $mission['date_interval']. 'days') -> format('Y-m-d');
                echo $end_date;
            } else if(isset($_POST['end'])) {
                echo htmlspecialchars($_POST['end']);
            } else {
                echo date('Y-m-d');
            }
            ?>" onkeyup="endDate(), minDate(), finalEndDate()" onchange="endDate(), minDate(), finalEndDate()" max="9999-12-31" required><br>

            <p id="finalEndDate" style="display: none;"></p>

            お小遣いの設定(この金額に応じて、ミッション達成時に獲得できる経験値が算出されます)<font color="red">＊</font><br>
            <input style="width:20%;" type="text" pattern="^[1-9][0-9]*$" name="price" value="<?php
            //mission_status.phpから受け取ったGETパラメータ
            if( isset($_GET['mission_id']) && mission_exists($pdo, 'mission', $_GET['mission_id']) ) {
                //GET有り
                echo htmlspecialchars($mission['reward']);
            } else if(isset($_POST['price'])) {
                echo htmlspecialchars($_POST['price']);
            }
            ?>" required>円<br>

            保護者様がご購入済みのサイト内通貨(設定可能な金額)：<?php
            //データ取得
            $result = $pdo -> query("SELECT * FROM parentbalance WHERE parent_id = '{$_COOKIE['parent_key']}'");
            

            if( $result -> rowCount() == 1 ) {
                $sql = "SELECT * FROM parentbalance WHERE parent_id = :parent_id";
                $stmt = $pdo -> prepare($sql);
                $stmt->bindValue(':parent_id', $_COOKIE['parent_key']);
                $stmt -> execute();

                $parent_info = $stmt -> fetch(PDO::FETCH_ASSOC);
                
                if($parent_info['parent_balance'] <= 0) {
                    echo '残高が有りません。';
                } else {
                    echo '￥'. number_format($parent_info['parent_balance']);
                }
            } else {
                echo '残高が有りません。';
            }
            ?><br>
            お子様の残高：<?php
            if(isset($_POST['child'])) {
                //データ取得
                $result = $pdo -> query("SELECT * FROM childbalance WHERE child_id = '{$_POST['child']}'");

                if( $result -> rowCount() == 1 ) {
                    $sql = "SELECT * FROM childbalance WHERE child_id = '{$_POST['child']}'";
                    $stmt = $pdo -> prepare($sql);
                    $stmt -> execute();
    
                    $child_info = $stmt -> fetch(PDO::FETCH_ASSOC);
                    
                    if($child_info['child_balance'] <= 0) {
                        echo '残高が有りません。';
                    } else {
                        echo '￥'. number_format($child_info['child_balance']);
                    }
                } else {
                    echo '残高が有りません。';
                }
            }
            ?><br><input type="submit" formaction="mission_post.php" formnovalidate value="残高を表示"><br>
            <input type="submit" formaction="money_parent.php" formnovalidate value="残高を追加"><br>

            <!-- <input type="submit" value="ミッション出題"> -->
            <?php
            //mission_status.phpから受け取ったGETパラメータ
            if( isset($_GET['mission_id']) && mission_exists($pdo, 'mission', $_GET['mission_id']) ) {
                //GET有り ?>
                <input type="hidden" name="mission_id" value="<?php echo $_GET['mission_id']; ?>">
                <input type="submit" name="update_mission" value="ミッション更新" <?php
                //残高が0なら、出題ボタン無効化
                $result = $pdo -> query("SELECT * FROM parentbalance WHERE parent_id = '{$_COOKIE['parent_key']}'");

                if( $result -> rowCount() == 1 ) {
                    $sql = "SELECT * FROM parentbalance WHERE parent_id = '{$_COOKIE['parent_key']}'";
                    $stmt = $pdo -> prepare($sql);
                    $stmt -> execute();
    
                    $parent_info = $stmt -> fetch(PDO::FETCH_ASSOC);
                    
                    if($parent_info['parent_balance'] <= 0) {
                        echo 'disabled';
                    }
                } else {
                    echo 'disabled';
                }
                ?>>
                <input type="submit" name="submit_mission" value="ミッションを更新せずに、新たにこのミッションを追加で出題" <?php
                //残高が0なら、出題ボタン無効化
                $result = $pdo -> query("SELECT * FROM parentbalance WHERE parent_id = '{$_COOKIE['parent_key']}'");

                if( $result -> rowCount() == 1 ) {
                    $sql = "SELECT * FROM parentbalance WHERE parent_id = '{$_COOKIE['parent_key']}'";
                    $stmt = $pdo -> prepare($sql);
                    $stmt -> execute();
    
                    $parent_info = $stmt -> fetch(PDO::FETCH_ASSOC);
                    
                    if($parent_info['parent_balance'] <= 0) {
                        echo 'disabled';
                    }
                } else {
                    echo 'disabled';
                }
                ?>>
            <?php } else {
                //GET無し ?>
                <input type="submit" name="submit_mission" value="ミッション出題" <?php
                //残高が0なら、出題ボタン無効化
                $result = $pdo -> query("SELECT * FROM parentbalance WHERE parent_id = '{$_COOKIE['parent_key']}'");

                if( $result -> rowCount() == 1 ) {
                    $sql = "SELECT * FROM parentbalance WHERE parent_id = '{$_COOKIE['parent_key']}'";
                    $stmt = $pdo -> prepare($sql);
                    $stmt -> execute();
    
                    $parent_info = $stmt -> fetch(PDO::FETCH_ASSOC);
                    
                    if($parent_info['parent_balance'] <= 0) {
                        echo 'disabled';
                    }
                } else {
                    echo 'disabled';
                }
                ?>>
            <?php } ?>
				</dd>
			</form>
        <div class="form-content">
            <a href="mission_status.php">
                <button class="button">ミッションステータス</button>
            </a>
        </div> 
        <footer class="footer">
            <p>&copy;Cent Disco</p>
        </footer>
    </main>
  </body>
</html>

<?php
//ページのリロード時にエラーメッセージ消去
//そのためにセッションを破棄

// // セッション変数を全て解除する
// $_SESSION = array();

// // セッションを切断するにはセッションクッキーも削除する。
// // Note: セッション情報だけでなくセッションを破壊する。
// if (ini_get("session.use_cookies")) {
//     $params = session_get_cookie_params();
//     setcookie(session_name(), '', time() - 42000,
//         $params["path"], $params["domain"],
//         $params["secure"], $params["httponly"]
//     );
// }

// // 最終的に、セッションを破壊する
// session_destroy();

unset($_SESSION['success']);
unset($_SESSION['child_id']);
unset($_SESSION['failed']);
unset($_SESSION['error']);