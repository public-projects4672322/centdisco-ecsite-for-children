<?php
$dsn = 'mysql:host=localhost;dbname=hew2022_it42107;charset=utf8mb4';
$db_user = 'hew2022_it42107';
$db_pass = '';

if(isset($_COOKIE['building_name'])){
  $address = $_COOKIE['pref01'].$_COOKIE['addr01'].$_COOKIE['house_number'].$_COOKIE['building_name'];
}else{
  $address = $_COOKIE['pref01'].$_COOKIE['addr01'].$_COOKIE['house_number'];
}


try{
  //$pdo = new PDO('mysql:dbname=hew2022_it42107;host=hew2022_it42107;charset=utf8','hew2022_it42107','');
  $pdo = new PDO($dsn, $db_user, $db_pass);
  $pdo->setAttribute(PDO::ATTR_ERRMODE,PDO::ERRMODE_EXCEPTION);
  $pdo->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);


  $sql = "SELECT parent_id FROM parents";
  $stmt = $pdo->prepare($sql);
  $stmt->execute();
  $all = $stmt->fetchAll();
  foreach($all as $loop){
    if($_COOKIE['parent_id'] == $loop['parent_id']){
      setcookie('err_parent_id', "保護者ユーザIDが重複しています。もう一度入力してください。", time() + 60 * 10);
      header('Location: regist_parent_1.php');
    }
  }

  $sql = "SELECT child_id FROM children";
  $stmt = $pdo->prepare($sql);
  $stmt->execute();
  $all = $stmt->fetchAll();
  foreach($all as $loop){
    if($_COOKIE['child_id'] == $loop['child_id']){
      setcookie('err_child_id', "子供ユーザIDが重複しています。もう一度入力してください。", time() + 60 * 10);
      header('Location: regist_child_1.php');
    }
  }


  $sql = "INSERT INTO `parents`(`parent_id`, `password`, `postal_code`, `address`, `parent_name`, `mail`) VALUES (:parent_id, :password, :postal_code, :address, :parent_name, :mail)";
  $prepare = $pdo->prepare($sql);
  $prepare->bindvalue(':parent_id', $_COOKIE['parent_id']);
  $prepare->bindvalue(':password', $_COOKIE['password']);
  $prepare->bindvalue(':postal_code', $_COOKIE['zip01']);
  $prepare->bindvalue(':address', $address);
  $prepare->bindvalue(':parent_name', $_COOKIE['parent_name']);
  $prepare->bindvalue(':mail', $_COOKIE['mail']);
  $prepare->execute();

  $sql = "INSERT INTO `children`(`child_id`, `parent_id`, `password`, `child_name`, `username`) VALUES (:child_id, :parent_id, :password, :child_name, :username)";
  $prepare = $pdo->prepare($sql);
  $prepare->bindvalue(':child_id', $_COOKIE['child_id']);
  $prepare->bindvalue(':parent_id', $_COOKIE['parent_id']);
  $prepare->bindvalue(':password', $_COOKIE['password_1']);
  $prepare->bindvalue(':child_name', $_COOKIE['child_name']);
  $prepare->bindvalue(':username', $_COOKIE['username']);
  $prepare->execute();
  header('Location: index.php');

  
  $sql = "INSERT INTO parentbalance VALUES(:parent_id, 0)";
  $qry = $pdo->prepare($sql);
  $qry->bindValue(':parent_id', $_COOKIE['parent_id']);
  $qry->execute();

  $sql = "INSERT INTO childbalance VALUES(:child_id, 0)";
  $qry = $pdo->prepare($sql);
  $qry->bindValue(':child_id', $_COOKIE['child_id']);
  $qry->execute();
  

} catch (PDOException $e) {
  echo 'DB接続エラー ： ' . $e->getMessage();
}


setcookie('child_id', "", time() - 60);
setcookie('password_1', "", time() - 60);
setcookie('child_name', "", time() - 60);
setcookie('username', "", time() - 60);
setcookie('parent_id', "", time() - 60);
setcookie('parent_name', "", time() - 60);
setcookie('mail', "", time() - 60);
setcookie('zip01', "", time() - 60);
setcookie('pref01', "", time() - 60);
setcookie('addr01', "", time() - 60);
setcookie('house_number', "", time() - 60);
setcookie('building_name', "", time() - 60);



?>
