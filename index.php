<!DOCTYPE html>
<html lang="ja">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="css/reset.css">
    <title>Cent Disco</title>
    <meta name="robots" content="none,noindex,nofollow">
</head>

<body class="body">
    <header class="header">
        <a href="index.php">
            <img src="images/logo001.png" alt="Cent Disco" class="header_logo">
        </a>
        <nav class="gnav">
            <ul class="menu">
                <li><a href="shop.php">Shop</a></li>
                <li><a href="login.php">MyPage&Login</a></li>
                <li><a href="contact.php">Contact</a></li>
                
                <li>
                    <a href="cart.php">
                        <img src="images/cart.png" alt="cart" class="header_cart">
                    </a>
                </li>
            </ul>
        </nav>
    </header>

    <div class="main-img"><img src="images/main_img.jpg" alt="" class="main-img__img"></div>
    <main class="main-content">
        <div class="message">
            <div class="message-top">
                <form class="form-content__form" action="login_check.php" method="post">
                    <dd class="form-content__submit"><input type="submit" value="ログイン & 会員登録"></dd>
                </form>
            </div>
            <div class="message-about">
                <p class="message-about__title">01 - about</p>
                <p class="message-about__text">キャッシュレスでもお金を可視化できるようにすることによって、見えないお金でも正しい金銭感覚を身につけるサポートをするサイトです。
                    また、ミッションシステムによって子供の様々な意欲を伸ばすお手伝いも可能です。
                </p>
            </div>
            <div class="message-about">
                <p class="message-about__title">02 - 取扱商品について</p>
                <p class="message-about__text">文房具やスポーツ用品、玩具などの子供向けの商品から保護者含めた家族で使うことのできる使える雑貨や家具など多岐にわたる品揃え
                </p>
            </div>
        </div>
        <hr>
        <h2 class="h2-title">おすすめ商品!!</h2>
        <div class="goods">
            <?php 
            $dsn = 'mysql:host=localhost;dbname=hew2022_it42107;charset=utf8mb4';
            $db_user = 'hew2022_it42107';
            $db_pass = '';
            
            try{
              $pdo = new PDO($dsn, $db_user, $db_pass);
              $pdo->setAttribute(PDO::ATTR_ERRMODE,PDO::ERRMODE_EXCEPTION);
              $pdo->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
            
              $sql = "SELECT * FROM products LIMIT 9";
              $qry = $pdo->prepare($sql);
              $qry->execute();
            
            } catch (PDOException $e) {
              echo 'DB接続エラー ： ' . $e->getMessage();
            }
            foreach($qry->fetchAll() as $q):?>
            <?php
            echo '<a href="shop_item.php?product_id=' . $q['product_id'] . '">';
            ?>
                <figure class="goods__item">
                    <?php
                    echo '<img src="images/shop/'. $q['image_path'] . '">';
                    ?>    
                    <figcaption>
                    <?php
                    echo $q['product_name'];
                    ?>    
                    </figcaption>
                </figure>
            </a>
            <?php endforeach;?>
        </div>

        <dl class="button-lineup">
            <dt class="button-lineup__title">NEXT PAGE</dt>
            <div class="button-lineup__buttonarea">
                <form action="shop.php">
                    <dd class="form-content__submit button-lineup__button"><input type="submit" value="Shop"></dd>
                </form>
                <form action="login.php">
                    <dd class="form-content__submit button-lineup__button"><input type="submit" value="Account & Login"></dd>
                </form>
                <form action="contact.php">
                    <dd class="form-content__submit button-lineup__button"><input type="submit" value="お問い合わせ"></dd>
                </form>
                <form action="">
                    <dd class="form-content__submit button-lineup__button"><input type="submit" value="その他"></dd>
                </form>
            </div>
        </dl>
    </main>
</body>

<footer class="footer">
    <p>&copy;Cent Disco</p>
</footer>


</html>