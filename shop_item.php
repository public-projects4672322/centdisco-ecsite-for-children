<?php
if (!isset($_GET['product_id'])) {
  header('Location: shop_item.php');
  exit;
}
$dsn = 'mysql:host=localhost;dbname=hew2022_it42107;charset=utf8mb4';
$db_user = 'hew2022_it42107';
$db_pass = '';
try {
  $pdo = new PDO($dsn, $db_user, $db_pass);
  $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
  $pdo->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);

  $sql = "SELECT * FROM products WHERE product_id = :product_id";
  $qry = $pdo->prepare($sql);
  $qry->bindValue('product_id', $_GET['product_id']);
  $qry->execute();
  $product = $qry->fetch();
  if (!$product) {
    header('Location: shop.php');
    exit;
  }
} catch (PDOException $e) {
  echo 'DB接続エラー ： ' . $e->getMessage();
}


?>
<!DOCTYPE html>
<html lang="ja">

<head>
  <meta charset="utf-8">
  <title>Cent Disco</title>
  <meta name="description" content="">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="robots" content="none,noindex,nofollow">
  <link rel="stylesheet" href="css/style_product.css">
  <link rel="stylesheet" href="css/style.css">
</head>

<body>
  <header class="header">
    <a href="index.php">
      <img src="images/logo001.png" alt="Cent Disco" class="header_logo">
    </a>
    <nav class="gnav">
      <ul class="menu">
        <li><a href="shop.php">Shop</a></li>
        <li><a href="login.php">MyPage&Login</a></li>
        <li><a href="contact.php">Contact</a></li>
        <li><a href="help.php">Help</a></li>
        <li>
          <a href="cart.php">
            <img src="images/cart.png" alt="cart" class="header_cart">
          </a>
        </li>
      </ul>
    </nav>
  </header>

  <main>
    <form method="get" action="cart.php">
      <!---->

      <div id="item" class="wrapper">
        <div class="item-image">
          <?php
          echo '<img src="images/shop/' . $product['image_path'] . '">';
          ?>
        </div>

        <div class="item-info">

          <option value="basketball">
            <!---->
            <h1 class="item-title"><?php echo $product['product_name'] ?></h1>
          </option>
          <!---->

          <p>
            <?php echo $product['introduction']; ?>
          </p>

          <p>&yen;<?php echo $product['price']; ?>(税込)</p>

          <p>数量</p>

          <table class="order-table">

            <tbody>
              <tr>
                <select name="quantity">
                  <option value="1">1</option>
                  <option value="2">2</option>
                  <option value="3">3</option>
                  <option value="4">4</option>
                  <option value="5">5</option>
                  <option value="6">6</option>
                  <option value="7">7</option>
                  <option value="8">8</option>
                  <option value="9">9</option>
                </select>
                </td>
              </tr>
            </tbody>

          </table>
          <!---->
          <?php
          echo '<input type="hidden" name="product_id" value="' . $product['product_id'] . '">';
          ?>
          <input type="submit" value="ADD TO CART" a class="cart-btn">
    </form>
    <!---->
    </div>
    </div>
  </main>

  <footer class="footer">
    <p>&copy;Cent Disco</p>
  </footer>

</body>

</html>