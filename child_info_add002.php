<?php
session_start();
$error_message = '';
if (isset($_SESSION['error'])) {
$error_message =$_SESSION['error'];
unset($_SESSION['error']);
}

if (isset($_POST['child_id'])) {
$a = $_POST['child_id'];
$b = $_POST['password'];
$c = $_POST['parent_id'];
}

if (isset($_GET['child_id'])) {
$a = $_GET['child_id'];
$b = $_GET['password'];
$c = $_GET['parent_id'];
}

if ($a == '' ||
$b == '') {
$_SESSION['error'] = '必須項目に空白があります';
header('Location: child_info_add001.php');
exit;
}

if(isset($_POST['password_1'])) {
if ($_POST['password_1'] != $_POST['password']) {
$_SESSION['error'] = 'パスワードが一致していません';
header('Location: child_info_add001.php');
exit;
}
}
?>
<!DOCTYPE html>
<html lang="jp">

<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="robot" content="none, noindex, nofollow">
  <link rel="stylesheet" href="css/reset.css">
  <link rel="stylesheet" href="css/style.css">
  <title>利用者会員登録</title>
  <script src="https://ajaxzip3.github.io/ajaxzip3.js" charset="UTF-8"></script>
  <meta name="robots" content="none,noindex,nofollow">
</head>
<body>
  <header class="header">
    <a href="index.php">
      <img src="images/logo001.png" alt="Cent Disco" class="header_logo">
    </a>
    <nav class="gnav">
      <ul class="menu">
        <li><a href="shop.php">Shop</a></li>
        <li><a href="login.php">MyPage&Login</a></li>
        <li><a href="contact.php">Contact</a></li>
        
        <li>
          <a href="cart.php">
            <img src="images/cart.png" alt="cart" class="header_cart">
          </a>
        </li>
      </ul>
    </nav>
  </header>

  <main class="main-content">
		<h2 class="body__title">SignUp-利用者会員登録</h2>
    <output name="result" style="color:red"><?php  echo $error_message;?></output><br>

    <form class="form-content"  method="post" action="child_info_add_confirm.php">
      <div class="form-content__subtitle">姓、名(漢字)<font color="red">　必須</font></div>
      <div class="form-content__input">
        <div class="">
          <input type="text" name="a">
          <input type="text" name="b">
        </div>
      </div>

      <div class="form-content__subtitle">姓、名(カナ)<font color="red">　必須</font></div>
      <div class="form-content__input">
        <div class="">
          <input type="text" name="c">
          <input type="text" name="d">
        </div>
      </div>

      <div class="form-content__subtitle">メールアドレス</div>
      <div class="form-content__input">
        <input type="email" name="mail" size="30" maxlength="50">
      </div>

      <div class="form-content__subtitle">郵便番号</div>
      <div class="form-content__input">
        <input type="text" name="zip01" size="10" maxlength="8" onKeyUp="AjaxZip3.zip2addr(this,'','pref01','addr01');">
      </div>

      <div class="form-content__subtitle">都道府県</div>
      <div class="form-content__input">
        <input type="text" name="pref01" size="20">
      </div>

      <div class="form-content__subtitle">市区町村</div>
      <div class="form-content__input">
        <input type="text" name="addr01" size="60">
      </div>

      <div class="form-content__subtitle">丁目 - 番地</div>
      <div class="form-content__input">
        <input type="text" name="q" size="60">
      </div>

      <div class="form-content__subtitle">建物名</div>
      <div class="form-content__input">
        <input type="text" name="w" size="60">
      </div>

      <input type="hidden" name="parent_id" value="<?php echo $c; ?>">
      <input type="hidden" name="child_id" value="<?php echo $a; ?>">
      <input type="hidden" name="password" value="<?php echo $b; ?>">
      <div class="form-content__submit"><input type="submit" value="次へ"></div>
      </form>

      <footer class="footer">
        <p>&copy;Cent Disco</p>
      </footer>

    </body>

    </html>
