<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>17.電子マネー管理、残高表示ページ</title>
    <link rel="stylesheet" href="css/reset.css">
    <link rel="stylesheet" href="css/style.css">
    <meta name="robots" content="none,noindex,nofollow">
</head>
<body>
    <header class="header">
        <a href="index.php">
        <img src="images/logo001.png" alt="Cent Disco" class="header_logo">
        </a>
        <nav class="gnav">
        <ul class="menu">
        <li><a href="shop.php">Shop</a></li>
        <li><a href="login.php">MyPage&Login</a></li>
        <li><a href="contact.php">Contact</a></li>
        
        <li>
        <a href="cart.php">
        <img src="images/cart.png" alt="cart" class="header_cart">
        </a>
        </li>
        </ul>
        </nav>
    </header>

    <div class="account_style">

    <h2>電子マネーの管理</h2>

    <dl>
    <dt>残高を表示したいお子様のアカウントを選択してください。</dt><br>
    <dd>
    <select name="type">
        <option value="id1">○○ ○○様-ID:○○○○○○-利用者専用アカウント</option>
        <option value="id2">○○ ○○様-ID:○○○○○○-利用者専用アカウント</option>
    </select>
    </dd>
    </dl>

    <dl>
    <dt>お子様の残高</dt><br>
    <dd>
    <input type="text">
    </dd>
    </dl>
    <br>

    <main id="botton">
        <button onclick="location.href='#####.html'">新たにミッションを出題</button>
    </main>
    <br>
    <main id="botton">
        <button onclick="location.href='#####.html'">MyPageへ戻る</button>
        <button onclick="location.href='#####.html'">ミッション一覧を表示</button>
    </main>
    </div>
    <footer class="footer">
        <p>&copy;Cent Disco</p>
    </footer>
</body>
</html>