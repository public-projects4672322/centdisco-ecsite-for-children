<?php
if (!isset($_COOKIE['child_key'])) {
    $_SESSION['message'] = 'ログインしてください。';
	header('Location: login.php');
    exit;
}

$dsn = 'mysql:host=localhost;dbname=hew2022_it42107;charset=utf8mb4';
$db_user = 'hew2022_it42107';
$db_pass = '';
$child_id = $_COOKIE['child_key'];
try{
    $pdo = new PDO($dsn, $db_user, $db_pass);
    $pdo->setAttribute(PDO::ATTR_ERRMODE,PDO::ERRMODE_EXCEPTION);
    $pdo->setAttribute(PDO::ATTR_EMULATE_PREPARES,false);
    $sql = "SELECT child_name,username FROM children WHERE child_id= :id";
    $prepare = $pdo->prepare($sql);
    $prepare->bindValue('id', $child_id);
    $prepare->execute();
    $user = $prepare->fetch(PDO::FETCH_ASSOC);
    if($user['username'] == ""){
        $user['username'] = $user['child_name'];
    }
    setcookie('child_username', $user['username'], time() + 60 * 60);


    $sql = "SELECT * FROM level";
    $qry = $pdo->prepare($sql);
    $qry->execute();
    $flg = 1;
    $level = 1;
    $icon_path = 'images/icon/icon_penguin01.png';
    for($i = 0; $i < 3; $i++){
        $badges[$i] = "x.png";
    }
    foreach($qry->fetchAll() as $q){
        if($q['child_id'] == $child_id){
            $level = $q['level'];
            if(!$q['icon_path'] == ""){
                $icon_path = 'images/icon/'.$q['icon_path'];
            }
            $sql2 = "SELECT * FROM badge WHERE badge_id = :badge_id";
            $qry2 = $pdo->prepare($sql2);
            for($i = 1; $i <= 3; $i++){
                $badge_id = 'badge_id_' . $i;
                $qry2->bindValue(':badge_id', $q[$badge_id]);
                $qry2->execute();
                $badge = $qry2->fetch();
               
                if(is_null($q[$badge_id]) || $q[$badge_id] == ''){
                    echo $i;
                }else{
                    $badges[$i-1] = $badge['badge_path'];
                }
                
            }
            $flg = 0;
            break;
        }
    }
    if($flg){
        $sql = "INSERT INTO `level`(`child_id`) VALUES (:child_id)";
        $qry = $pdo->prepare($sql);
        $qry->bindValue(':child_id', $child_id);
        $qry->execute();
    }



}catch(PDOException $e){
    echo $e->getMessage();
    exit;
}
?>
<!DOCTYPE html>
<html lang="ja">

<head>

    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="css/reset.css">
        <link rel="stylesheet" href="css/style.css">
        <meta name="robots" content="none,noindex,nofollow">
        <title>Cent Disco | MyPage</title>
    </head>
</head>

<body class="body">
    <header class="header">
        <a href="index.php">
            <img src="images/logo001.png" alt="Cent Disco" class="header_logo">
        </a>
        <nav class="gnav">
            <ul class="menu">
                <li><a href="shop.php">Shop</a></li>
                <li><a href="login.php">MyPage&Login</a></li>
                <li><a href="contact.php">Contact</a></li>
                
                <li>
                    <a href="cart.php">
                        <img src="images/cart.png" alt="cart" class="header_cart">
                    </a>
                </li>
            </ul>
        </nav>
    </header>
    <main class="mypage_child">
        <div class="mypage_child__top">
            <h1>MyPage-マイページ</h1>
            <a href="logout.php">
                <p class="logout_text">ログアウト</p><img src="images/icon_mono/logout.png" class="logout_img">
            </a>
        </div>
        
        <div class="child-status">
            <div class="child-status__icon">
                <img class="child-status__icon-img" src="<?php echo $icon_path; ?>" alt="アイコン画像">
            </div>
            <div class="child-status__description">
                <h3><?php echo $user['username']; ?><span style="font-size:20px;margin-left:0;">さん</span> <span>Lv<?php echo $level;?></span></h3>
                <div class="child-status__badge-area">
                <?php
                for($i = 0; $i < 3; $i++){
                    echo '<img class="child-status__badge" src="images/badges/' . $badges[$i] . '">';
                }
                ?>  
                    
                </div>
            </div>
        </div>

        <h2>ユーザー情報</h2>
        <div class="user-info">
            <div class="user-info_class">
                <a href="child_change.php">
                    <img src="images/icon_mono/personal_data.png" class="img" alt="">
                    <h3>プロフィールの編集</h3>
                    <p>あなたのユーザ名、アイコンを編集します。</p>
                </a>
            </div>
            <div class="user-info_class">
                <a href="child_customize.php">
                    <img src="images/icon_mono/palet.png" class="img" alt="">
                    <h3>カスタマイズ</h3>
                    <p>あなたのバッジやフォント(文字のデザイン)、壁紙を変えることができます。</p>
                </a>
            </div>
            <div class="user-info_class">
                <a href="money_mypage_child.php">
                    <img src="images/icon_mono/money_icon.png" class="img" alt="">
                    <h3>電子マネー(お金)の管理</h3>
                    <p>このサイトだけでご利用いただけるお金の管理を行います。</p>        
                </a>
            </div>
            <div class="user-info_class">
                <a href="not_yet.php">
                    <img src="images/icon_mono/task_manage.png" class="img" alt="">
                    <h3>ミッションの管理</h3>
                    <p>出題されているミッションを表示します。管理もここで行います。</p>
                </a>
            </div>
        </div>
        <hr>
        <h2>ショッピング情報</h2>
        <div class="shopping-info">
            <div class="shopping-info_class">
                <a href="not_yet.php">
                    <img src="images/icon_mono/receipt.png" class="img" alt="">
                    <h3>購入したもの</h3>
                    <p>購入したものを見ることができます。</p>
                </a>
            </div>
            <div class="shopping-info_class">
                <a href="not_yet.php">
                    <img src="images/icon_mono/hourglass.png" class="img" alt="">
                    <h3>購入承認待ち</h3>
                    <p>発送されていない商品を見ることができます。保護者の方からの許可を待ちましょう。</p>
                </a>
            </div>
        </div>
        <hr>
        <h2>管理情報</h2>
        <div class="management-info">
            <div class="management-info_class">
                <a href="not_yet.php">
                    <img src="images/icon_mono/door.png" class="img" alt="">
                    <h3>退会</h3>
                    <p>あなたのアカウントの削除を行います。<br>保護者の方の承認が必要です。</p>
                </a>
            </div>
        </div>
    </main>
    
    <footer class="footer">
        <p>&copy;Cent Disco</p>
    </footer>
</body>

</html>