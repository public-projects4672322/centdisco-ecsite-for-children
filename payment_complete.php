<?php
setcookie('products[]', '', time() - 60);
setcookie('quantity[]', '', time() - 60);
setcookie('cart', '', time() - 60);
?>
<!DOCTYPE html>
<html lang="ja">

<head>
    <meta charset="UTF-8">
    <title>cent disco | 支払完了画面</title>
    <link rel="stylesheet" href="css/reset.css">
</head>

<body>
    <style>
        body {
            width: 100%;
            margin: 0 auto;
            max-width: 1420px;
            min-width: 600px;
        }

        .box {
            text-align: center;
            width: 50%;
            height: 100px;
            padding: 0.5em 1em;
            margin: 48px auto;
            border: double 5px #4ec4d3;
        }

        .box p {
            margin-top: 8px;
            padding: 0;
        }

        .btn a {
            display: block;
            position: relative;
            margin: 18px auto 0 auto;
            padding: 1em 2em;
            width: 170px;
            color: #333;
            font-size: 18px;
            font-weight: 700;
            background-color: #cccccc;
            transition: 0.3s;
        }

        .btn a::before {
            content: '';
            position: absolute;
            top: 50%;
            right: 2em;
            transform: translateY(-50%) rotate(45deg);
            width: 5px;
            height: 5px;
            border-top: 2px solid #333333;
            border-right: 2px solid #333333;
            transition: 0.3s;
        }

        .btn a::after {
            content: '';
            position: absolute;
            top: 50%;
            right: calc(2em + 5px);
            transform: translateY(-50%) rotate(45deg);
            width: 5px;
            height: 5px;
            border-top: 2px solid #333333;
            border-right: 2px solid #333333;
            transition: 0.3s;
        }

        .btn a:hover {
            text-decoration: none;
            background-color: #bbbbbb;
        }

        .btn a:hover::before {
            right: 1.8em;
        }

        .btn a:hover::after {
            right: calc(1.8em + 5px);
        }
    </style>
    <div class="box">
        <p>ご購入ありがとうございました。</p>
        <div class="btn">
            <a href="mypage_child.php">マイページへ戻る</a>
        </div>
    </div>
</body>

</html>