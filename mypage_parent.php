<?php
if (!isset($_COOKIE['parent_key'])) {
    $_SESSION['message'] = 'ログインしてください。';
	header('Location: login.php');
    exit;
}

$dsn = 'mysql:host=localhost;dbname=hew2022_it42107;charset=utf8mb4';
$db_user = 'hew2022_it42107';
$db_pass = '';
$parent_id = $_COOKIE['parent_key'];
try{
    $pdo = new PDO($dsn, $db_user, $db_pass);
    $pdo->setAttribute(PDO::ATTR_ERRMODE,PDO::ERRMODE_EXCEPTION);
    $pdo->setAttribute(PDO::ATTR_EMULATE_PREPARES,false);
    $sql = "SELECT parent_name FROM parents WHERE parent_id= :id";
    $prepare = $pdo->prepare($sql);
    $prepare->bindValue(':id', $parent_id);
    $prepare->execute();
    $user = $prepare->fetch(PDO::FETCH_ASSOC);
    setcookie('parent_name', $user['parent_name'], time() + 60 * 60);

    $sql = "SELECT * FROM children WHERE parent_id = :parent_id";
    $qry = $pdo->prepare($sql);
    $qry->bindValue(':parent_id', $_COOKIE['parent_key']);
    $qry->execute();


}catch(PDOException $e){
    echo $e->getMessage();
    exit;
}

?>
<!DOCTYPE html>
<html lang="ja">

<head>

    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="css/reset.css">
        <link rel="stylesheet" href="css/style.css">
        <meta name="robots" content="none,noindex,nofollow">
        <title>Cent Disco | MyPage</title>
    </head>
</head>

<body class="body">
    <header class="header">
        <a href="index.php">
            <img src="images/logo001.png" alt="Cent Disco" class="header_logo">
        </a>
        <nav class="gnav">
            <ul class="menu">
                <li><a href="shop.php">Shop</a></li>
                <li><a href="login.php">MyPage&Login</a></li>
                <li><a href="contact.php">Contact</a></li>
                
                <li>
                    <a href="cart.php">
                        <img src="images/cart.png" alt="cart" class="header_cart">
                    </a>
                </li>
            </ul>
        </nav>
    </header>
    <main class="mypage_child">
        <div class="mypage_child__top">
            <h1>MyPage-マイページ</h1>
            <a href="logout.php">
                <p class="logout_text">ログアウト</p><img src="images/icon_mono/logout.png" class="logout_img">
            </a>
        </div>
        
        <div class="parent-status">
            <div class="parent-status__name">
                <h3><?php echo $user['parent_name']; ?><span> 様</span></h3>
            </div>
            <div class="parent-status__child">
                <h4 class="parent-status__child-title">紐づけされているお子様</h4>
                <?php
                foreach($qry->fetchAll() as $q){
                    echo '<h4 class="parent-status__child-name">&#9675;';
                    echo $q['child_name'];
                    echo '</h4>';
                }
                ?>
            </div>
        </div>

        <h2>ユーザー情報</h2>
        <div class="user-info">
            <div class="user-info_class">
                <a href="parent_info.php">
                    <img src="images/icon_mono/info.png" class="img" alt="">
                    <h3>あなたの個人情報</h3>
                    <p>あなたの個人情報の閲覧・編集ができます。</p>
                </a>
            </div>
            <div class="user-info_class">
                <a href="child_info.php">
                    <img src="images/icon_mono/child.png" class="img" alt="">
                    <h3>お子様の個人情報</h3>
                    <p>あなたの紐づけされているお子様の個人情報の閲覧・編集ができます。</p>
                </a>
            </div>
            <div class="user-info_class">
                <a href="money_parent.php">
                    <img src="images/icon_mono/money_icon.png" class="img" alt="">
                    <h3>電子マネー(お金)の管理</h3>
                    <p>このサイトだけでご利用いただけるお金の管理を行います。</p>        
                </a>
            </div>
            <div class="user-info_class">
                <a href="mission_status.php">
                    <img src="images/icon_mono/task_manage.png" class="img" alt="">
                    <h3>ミッションの管理</h3>
                    <p>出題されているミッションを表示します。管理もここで行います。</p>
                </a>
            </div>
        </div>
        <hr>
        <h2>ショッピング情報</h2>
        <div class="shopping-info">
            <div class="shopping-info_class">
                <a href="">
                    <img src="images/icon_mono/receipt.png" class="img" alt="">
                    <h3>購入したもの</h3>
                    <p>お子様が過去に購入したものを見ることができます。</p>
                </a>
            </div>
            <div class="shopping-info_class">
                <a href="">
                    <img src="images/icon_mono/hourglass.png" class="img" alt="">
                    <h3>購入承認待ち</h3>
                    <p>購入承認待ちの商品はこちらでご覧になれます。承認もこちらからできます。</p>
                </a>
            </div>
        </div>
        <hr>
        <h2>管理情報</h2>
        <div class="management-info">
            <div class="management-info_class">
                <a href="">
                    <img src="images/icon_mono/door.png" class="img" alt="">
                    <h3>退会</h3>
                    <p>あなたのアカウントの削除を行います。</p>
                </a>
            </div>
        </div>
    </main>
    
    <footer class="footer">
        <p>&copy;Cent Disco</p>
    </footer>
</body>

</html>