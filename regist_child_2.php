<?php
session_start();
$error_message = '';
if (isset($_SESSION['error'])) {
$error_message =$_SESSION['error'];
unset($_SESSION['error']);
}

if (isset($_POST['child_id'])) {
$a = $_POST['child_id'];
$b = $_POST['password'];
setcookie('child_id', $a, time() + 60 * 30);
setcookie('password_1', $b, time() + 60 * 30);
}



if(isset($_POST['password_1'])) {
if ($_POST['password_1'] != $_POST['password']) {
$_SESSION['error'] = 'パスワードが一致していません';
header('Location: regist_child_1.php&parent_id='.$c);
exit;
}
}
?>
<!DOCTYPE html>
<html lang="jp">

<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="robot" content="none, noindex, nofollow">
  <link rel="stylesheet" href="css/reset.css">
  <link rel="stylesheet" href="css/style.css">
  <title>利用者会員登録</title>
  <script src="https://ajaxzip3.github.io/ajaxzip3.js" charset="UTF-8"></script>
  <meta name="robots" content="none,noindex,nofollow">
</head>
<body>
  <header class="header">
    <a href="index.php">
      <img src="images/logo001.png" alt="Cent Disco" class="header_logo">
    </a>
    <nav class="gnav">
      <ul class="menu">
        <li><a href="shop.php">Shop</a></li>
        <li><a href="login.php">MyPage&Login</a></li>
        <li><a href="contact.php">Contact</a></li>
        
        <li>
          <a href="cart.php">
            <img src="images/cart.png" alt="cart" class="header_cart">
          </a>
        </li>
      </ul>
    </nav>
  </header>

  <main class="main-content">
		<h2 class="body__title">SignUp-利用者会員登録</h2>
    <output name="result" style="color:red"><?php  echo $error_message;?></output><br>

    <form class="form-content"  method="post" action="regist_child_confirm.php">
      <div class="form-content__subtitle">氏名<font color="red">　必須</font></div>
      <div class="form-content__input">
        <input type="text" name="child_name">
      </div>
      <div class="form-content__subtitle">ユーザネーム</div>
      <div class="form-content__input">
        <input type="text" name="username">
      </div>

      <div class="form-content__submit"><input type="submit" value="次へ"></div>
      </form>
      <footer class="footer">
        <p>&copy;Cent Disco</p>
      </footer>
    </body>
</html>
