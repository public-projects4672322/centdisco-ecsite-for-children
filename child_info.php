<?php
if (!isset($_COOKIE['parent_key'])) {
    $_SESSION['message'] = 'ログインしてください。';
    header('Location: login.php');
    exit;
}

$dsn = 'mysql:host=localhost;dbname=hew2022_it42107;charset=utf8mb4';
$db_user = 'hew2022_it42107';
$db_pass = '';
$parent_id = $_COOKIE['parent_key'];
try {
    $pdo = new PDO($dsn, $db_user, $db_pass);
    $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $pdo->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);

    $sql = "SELECT * FROM children WHERE parent_id = :parent_id";
    $qry = $pdo->prepare($sql);
    $qry->bindValue(':parent_id', $_COOKIE['parent_key']);
    $qry->execute();
} catch (PDOException $e) {
    echo $e->getMessage();
    exit;
}

?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Cent Disco | 子供用アカウント管理ページ</title>
    <link rel="stylesheet" href="css/reset.css">
    <link rel="stylesheet" href="css/style.css">
    <meta name="robots" content="none,noindex,nofollow">
</head>

<body>
    <header class="header">
        <a href="index.php">
            <img src="images/logo001.png" alt="Cent Disco" class="header_logo">
        </a>
        <nav class="gnav">
            <ul class="menu">
                <li><a href="shop.php">Shop</a></li>
                <li><a href="login.php">MyPage&Login</a></li>
                <li><a href="contact.php">Contact</a></li>

                <li>
                    <a href="cart.php">
                        <img src="images/cart.png" alt="cart" class="header_cart">
                    </a>
                </li>
            </ul>
        </nav>
    </header>

    <main class="main-content">

        <h1 class="body__title">お子様アカウントの管理</h1>
        <p></p>

        <dl class="form-content">
            <dt class="form-content__title" style="height:150px;">Top-管理画面</dt>
            <dd class="form-content__description">
                <p>以下、保護者のアカウントに紐付けられるお子様のアカウントです。</p>
            </dd>

            <?php
            $i = 1;
            foreach ($qry->fetchAll() as $q) {
                echo '<dt class="form-content__subtitle account_info">0' . $i . '-アカウント情報</dt>';
                echo '<dd class="form-content__input">';
                $child_name = $q['child_name'];
                $child_id = $q['child_id'];

                echo '<input value="' . $child_name . ' 様" disabled>';
                echo '</dd>';


                echo '<dt class="form-content__subtitle account_info">パスワード</dt>';
                echo '<dd class="form-content__input account_info">';
                echo "<input value='" . $q['password'] . "' type='password' disabled>";
                echo '</dd>';

                echo '<dt class="form-content__subtitle account_info" style="margin-bottom: 100px;">ユーザネーム</dt>';
                echo '<dd class="form-content__input account_info">';
                echo "<input value='" . $q['username'] . "' disabled>";
                echo '</dd>';
                $i++;
            }
            ?>
            <form action="mypage_parent.php">
                <dd class="form-content__submit"><input type="submit" value="マイページへ"></dd>
            </form>


        </dl>

        <!-- <table>
            <th></th>
        </table>
        <select name="ID" id="ID">
            <option value="詳細">クリックして詳細を表示</option>
            <option value="内容1">内容1</option>
            <option value="内容2">内容2</option>
        </select>
        <main id="botton">
            <button onclick="location.href='11.会員情報提示ページ.html'">編集</button>
            <button onclick="location.href='34.###.html'">このアカウントの削除</button>
        </main>

        <table>
            <th>〇〇 〇〇様-ID:〇〇〇〇〇〇-お子様用アカウント</th>
        </table>
        <select name="ID" id="ID">
            <option value="詳細">クリックして詳細を表示</option>
            <option value="内容1">内容1</option>
            <option value="内容2">内容2</option>
        </select>
        <main id="botton">
            <button onclick="location.href='11.会員情報提示ページ.html'">編集</button>
            <button onclick="location.href='34.###.html'">このアカウントの削除</button>
        </main>

        <table>
            <th>〇〇 〇〇様-ID:〇〇〇〇〇〇-お子様用アカウント</th>
        </table>
        <select name="ID" id="ID">
            <option value="詳細">クリックして詳細を表示</option>
            <option value="内容1">内容1</option>
            <option value="内容2">内容2</option>
        </select>
        <main id="botton">
            <button onclick="location.href='11.会員情報提示ページ.html'">編集</button>
            <button onclick="location.href='34.###.html'">このアカウントの削除</button>
        </main>

        <main id="botton">
            <button onclick="location.href='14.子供用アカウント追加登録ページ1.html'">編集</button>
        </main>

        <main id="botton">
            <button onclick="location.href='mypage_parent.php'">MyPageへ戻る</button>
        </main>

        </div> -->
    </main>


    <footer class="footer">
        <p>&copy;Cent Disco</p>
    </footer>
</body>

</html>