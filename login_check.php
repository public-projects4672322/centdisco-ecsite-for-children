<?php
$id = $_POST['id'];
$pass = $_POST['password'];
session_start();
//URL直打ちチェック
if(!isset($id,$pass)){
    $_SESSION['message'] = 'IDとパスワードを入力してください。';
	header('Location: login.php');
    exit;
}
//必須入力チェック
if($id == '' || $pass == ''){
    $_SESSION['message'] = 'ID及びPasswordは必須入力項目です。';
	header('Location: login.php');
    exit;
}
//子供or保護者判定
$dsn = 'mysql:host=localhost;dbname=hew2022_it42107;charset=utf8mb4';
$db_user = 'hew2022_it42107';
$db_pass = '';
if ($_POST['access'] == 1) {
    try{
        $pdo = new PDO($dsn, $db_user, $db_pass);
        $pdo->setAttribute(PDO::ATTR_ERRMODE,PDO::ERRMODE_EXCEPTION);
        $pdo->setAttribute(PDO::ATTR_EMULATE_PREPARES,false);
        $sql = "SELECT * FROM children WHERE child_id = :id";
        $prepare = $pdo->prepare($sql);
        $prepare->bindValue('id', $id);
        $prepare->execute();
        if ($user = $prepare->fetch(PDO::FETCH_ASSOC)) {
            if ($pass = $user['password']) {
                setcookie(
                    "child_key", //キー名称
                    $user['child_id'], //データ
                    time() + 60 * 60 * 5
                );
                header('Location: mypage_child.php');
                exit;
            }
        }
    }catch(PDOException $e){
        echo $e->getMessage();
        exit;
    }
    $_SESSION['message'] = 'ID、パスワードが正しくありません。もう一度入力してください。';
    header('Location: login.php');
    exit;
} else {
    try{
        $pdo = new PDO($dsn, $db_user, $db_pass);
        $pdo->setAttribute(PDO::ATTR_ERRMODE,PDO::ERRMODE_EXCEPTION);
        $pdo->setAttribute(PDO::ATTR_EMULATE_PREPARES,false);
        $sql = "SELECT * FROM parents WHERE parent_id= :id";
        $prepare = $pdo->prepare($sql);
        $prepare->bindValue('id', $id);
        $prepare->execute();
        if ($user = $prepare->fetch(PDO::FETCH_ASSOC)) {
            if ($pass = $user['password']) {
                setcookie(
                    "parent_key", //キー名称
                    $user['parent_id'], //データ
                    time() + 60 * 60 * 5
                );
                header('Location: mypage_parent.php');
                exit;
            }
        }
    }catch(PDOException $e){
        echo $e->getMessage();
        exit;
    }
    $_SESSION['message'] = 'ID、パスワードが正しくありません。もう一度入力してください。';
    header('Location: login.php');
    exit;
}
