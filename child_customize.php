<?php
session_start();
$message = "";
if (!isset($_COOKIE['child_key'])) {
  $_SESSION['message'] = 'ログインしてください。';
  header('Location: login.php');
  exit;
}

$dsn = 'mysql:host=localhost;dbname=hew2022_it42107;charset=utf8mb4';
$db_user = 'hew2022_it42107';
$db_pass = '';

try {
  $pdo = new PDO($dsn, $db_user, $db_pass);
  $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
  $pdo->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);

  $sql = "SELECT * FROM badge";
  $qry = $pdo->prepare($sql);
  $qry->execute();
} catch (PDOException $e) {
  echo 'DB接続エラー ： ' . $e->getMessage();
}


?>
<!DOCTYPE html>
<html lang="ja">

<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="stylesheet" href="css/reset.css">
  <link rel="stylesheet" href="css/style.css">
  <title>Cent Disco</title>
  <meta name="robots" content="none,noindex,nofollow">
  <script type='text/javascript' src='//ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js?ver=1.11.3'></script>
</head>

<body class="body">
  <header class="header">
    <a href="index.php">
      <img src="images/logo001.png" alt="Cent Disco" class="header_logo">
    </a>
    <nav class="gnav">
      <ul class="menu">
        <li><a href="shop.php">Shop</a></li>
        <li><a href="login.php">MyPage&Login</a></li>
        <li><a href="contact.php">Contact</a></li>
        <li>
          <a href="cart.php">
            <img src="images/cart.png" alt="cart" class="header_cart">
          </a>
        </li>
      </ul>
    </nav>
  </header>

  <main class="main-content">
    <h2 class="body__title">MyPage - カスタマイズ</h2>

    <dl class="form-content">
      <output style="color:red; margin-bottom:30px;"><?php echo $message; ?></output>
      <form class="form-content" method="post" action="child_customize_check.php">
        <dt class="form-content__subtitle">01 バッジ選択</dt>
        <dd class="form-content__radio change-badge">
          <p>最大3つまで選択可能</p><br>
          <?php
          $i = 0;
          foreach ($qry->fetchAll() as $badge) {
            if ($i == 0) {
              echo '<div><input type="checkbox" name="badge[]" id="' . $badge['badge_id'] . '" value="' . $badge['badge_id'] . '" checked>';
            } else {
              echo '<div><input type="checkbox" name="badge[]" id="' . $badge['badge_id'] . '" value="' . $badge['badge_id'] . '">';
            }
            echo '<label for="' . $badge['badge_id'] . '">';
            echo '<img class="change-badge_img" src="images/badges/' . $badge['badge_path'] . '">';
            echo '</label></div>';
            $i++;
          }


          ?>
        </dd>
        <a href="mypage_child.php">
          <button class="button">マイページへ</button>
        </a>
        <input type="hidden" name="badge_change" value="true">
        <dd class="form-content__submit"><input type="submit" value="編集する"></dd>
      </form>
    </dl>
  </main>

  <footer class="footer">
    <p>&copy;Cent Disco</p>
  </footer>

  <script>
    $("input[type=checkbox]").click(function() {
      var $count = $("input[type=checkbox]:checked").length;
      var $not = $('input[type=checkbox]').not(':checked')

      //チェックが3つ付いたら、チェックされてないチェックボックスにdisabledを加える
      if ($count >= 3) {
        $not.attr("disabled", true);
      } else {
        //3つ以下ならisabledを外す
        $not.attr("disabled", false);
      }
    });
  </script>

</body>

</html>