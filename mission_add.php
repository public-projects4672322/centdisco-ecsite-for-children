<?php
session_start();

//追加カラム：mission_id、child_date（子供id）、date_interval（間隔 単位は日にち(day)）、permanent（永続->Yes / 回数指定有りor指定なし->No）、default_count(回数指定時の回数初期値)

/*
ミッション達成判定(missionテーブルにおいて)
・valid = no（回数指定なしのものを達成 または 指定回数分、すべて達成）
・valid = yes かつ default_count > count（1回 ～ default_count - count 回達成）
（countはミッション一回達成(承認)ごとに減算）
・valid = yes かつ default_count = null かつ count < 0（最低1回は達成）
上のいずれかの場合とする

countがnullや0なら「valid = no」で承認済みとする
countが1以上ならcountから1だけ減算 (validはyesのまま）

例えばdefault_count = 3、 count = 1であれば、そのミッションは2回達成済み

回数指定が永続(permanent = yes, count = null)の場合は、どのような場合でもvalidがyesにはならない
count のマイナスの値が達成回数
例えばcount = -2であれば、永続のミッションを2回達成
*/

//ミッション登録時、更新時共に 指定回数 == count == default_countとするため、
//そのときミッションを達成した回数はリセットされる

//終了日(回数指定時の最終的な)はstart_dateとdate_intervalから算出

//直リンクチェック
if( !isset($_POST['submit_mission']) && !isset($_POST['update_mission']) ) {
    header('Location: index.php');
    exit;
}

//テーブル存在チェック関数
function table_exists($pdo, $table) {
    $result = $pdo -> query("SHOW TABLES LIKE '{$table}'");

    if( $result -> rowCount() == 1 ) {
        return true;
    } else {
        return false;
    }
}

//カラム存在チェック関数
function column_exists($pdo, $table) {
    $result = $pdo -> query("SELECT * FROM $table /* WHERE deleted_at IS NULL */");

    if( $result -> rowCount() > 0 ) {
        return true;
    } else {
        return false;
    }
}

//ミッション存在チェック関数
function product_exists($pdo, $table, $id) {
    $result = $pdo -> query("SELECT * FROM $table WHERE mission_id = $id /* AND deleted_at IS NULL */");

    if( $result -> rowCount() == 1 ) {
        return true;
    } else {
        return false;
    }
}

// DB登録 //////////////////////////////////////////////////////////////////////////////////////
try {
    //DSN(Data Source Name)…DBへの接続情報
    $dsn = 'mysql:host=localhost;dbname=hew2022_it42107;charset=utf8mb4';
    $db_user = 'hew2022_it42107';
    $db_pass = '';

    //DB接続(open) イメージコマンドのmysql -u root -p xxx
    $pdo = new PDO($dsn, $db_user, $db_pass);

    //デフォルトの動作はエラー黙殺
    //→エラー時、そのアラートを発生するようにする
    $pdo -> setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    //DB存在チェック(パーミッションがあること前提)
    $pdo -> query("CREATE DATABASE IF NOT EXISTS hew2022_it42107");

    $dsn = 'mysql:host=localhost;dbname=hew2022_it42107;charset=utf8mb4';
    $pdo = new PDO($dsn, $db_user, $db_pass);

    if( !table_exists($pdo, 'mission') ) {
        //テーブルが存在しない場合作成
        $pdo -> query("CREATE TABLE mission (
            mission_id int NOT NULL,
            parent_id varchar(8) NOT NULL,
            child_id varchar(8) NOT NULL,
            create_at datetime NOT NULL DEFAULT current_timestamp(),
            mission_name varchar(64) NOT NULL,
            mission_detail varchar(255) DEFAULT NULL,
            start_date date NOT NULL,
            date_interval int NOT NULL,
            reward int(11) NOT NULL,
            count int DEFAULT NULL,
            default_count int DEFAULT NULL,
            permanent varchar(3) NOT NULL DEFAULT 'no',
            valid varchar(3) NOT NULL DEFAULT 'yes'
        ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;");

        $pdo -> query("ALTER TABLE mission ADD PRIMARY KEY (mission_id);");

        $pdo -> query("ALTER TABLE mission MODIFY mission_id int NOT NULL AUTO_INCREMENT;");

        $pdo -> query("COMMIT;");
    }

    //親ID取得
    $sql = "SELECT parent_id FROM children WHERE child_id = '{$_POST['child']}'";
    $stmt = $pdo -> prepare($sql);
    $stmt -> execute();

    $parent = $stmt -> fetch(PDO::FETCH_ASSOC);
    // print_r($parent);

    //保護者の残高が足りなければ強制終了
    $sql = "SELECT * FROM parentbalance WHERE parent_id = '{$parent['parent_id']}'";
    $stmt = $pdo -> prepare($sql);
    $stmt -> execute();

    $parent_info = $stmt -> fetch(PDO::FETCH_ASSOC);
    
    if($parent_info['parent_balance'] < $_POST['price']) {
        $_SESSION['error'] = '保護者様の残高が足りません。ミッションの出題に失敗しました。入力内容を再度表示するには、ブラウザバックしてください。<br>「フォーム再送信の確認」という画面が表示されますが、その画面で再読み込みボタンを押してください。';

        header('Location: mission_post.php');
        exit;
    }

    //SQL文作成
    if( isset($_POST['submit_mission']) ) {
        $sql = "INSERT INTO mission(parent_id, child_id, create_at, mission_name, mission_detail, start_date, date_interval, reward, count, default_count, permanent) VALUES(:parent_id, :child_id, current_timestamp(), :mission_name, :mission_detail, :start_date, :date_interval, :reward, :count, :default_count, :permanent)";
    } else if( isset($_POST['update_mission']) ) {
        $sql = "UPDATE mission SET parent_id = :parent_id, child_id = :child_id, mission_name = :mission_name, mission_detail = :mission_detail, start_date = :start_date, date_interval = :date_interval, reward = :reward, count = :count, default_count = :default_count, permanent = :permanent WHERE mission_id = :mission_id";
    }

    //プリペアードステートメントの取得
    $prepare = $pdo -> prepare($sql);

    //プレースホルダへの値の設定
    $prepare -> bindValue(':parent_id', $parent['parent_id']);

    $prepare -> bindValue(':child_id', $_POST['child']);

    $prepare -> bindValue(':mission_name', $_POST['name']);

    $prepare -> bindValue(':mission_detail', $_POST['detail']);
    if(!isset($_POST['detail']) || $_POST['detail'] == '') {
        //ミッション内容入力無し
        $_POST['detail'] = '【設定なし】';
    }

    $prepare -> bindValue(':start_date', $_POST['start']);

    //開始日
    $start_date = new DateTime($_POST['start']);
    //終了日
    $end_date = ( new DateTime($_POST['start']) ) -> modify("+". $_POST['period_number']. $_POST['interval']);
    // $start_date = $start_date -> format('Y-m-d');
    // $end_date = $end_date -> format('Y-m-d');
    // echo '開始日：'. $start_date. '終了日'. $end_date;
    //日数差
    $diff = $end_date -> diff($start_date);
    $interval = $diff -> days;
    // echo '日数差：'. $interval;
    $prepare -> bindValue(':date_interval', $interval);

    $prepare -> bindValue(':reward', $_POST['price']);

    $prepare -> bindValue(':count', null);
    $prepare -> bindValue(':default_count', null);

    if( isset($_POST['always']) ) {
        //回数指定無し count = null
        //永続 permanent = yes
        $prepare -> bindValue(':count', $_POST['count']);
        $prepare -> bindValue(':default_count', $_POST['count']);

        $count = '【永続】';
    } else if(!isset($_POST['times']) || $_POST['count'] == null) {
        //回数指定無し count = null
        //非永続 permanent = no
        $count = '【設定なし】';
    } else {
        //回数指定有り count = 999
        //非永続 permanent = no
        $prepare -> bindValue(':count', $_POST['count']);
        $prepare -> bindValue(':default_count', $_POST['count']);

        $count = $_POST['count']. '回';
    } 

    $prepare -> bindValue(':permanent', 'no');
    if( isset($_POST['always']) ) {
        $prepare -> bindValue(':permanent', 'yes');
    }

    if( isset($_POST['update_mission']) ) {
        $mission_id = $_POST['mission_id'];
        $prepare -> bindValue(':mission_id', $mission_id);
    }
    
    //SQL実行
    $prepare -> execute();

    //ミッションidの取得
    $mission_id = $pdo -> lastInsertId();
    if( isset($_POST['update_mission']) ) {
        $mission_id = $_POST['mission_id'];
    }

    //表示の修正
    //年月日
    $date1 = $start_date;   //曜日出力のため一時的に格納
    $start_date = $start_date -> format('Y年n月j日');
    $date2 = $end_date;     //曜日出力のため一時的に格納
    $end_date = $end_date -> format('Y年n月j日');
    //曜日
    $weekday = ['(日)', '(月)', '(火)', '(水)', '(木)', '(金)', '(土)'];
    $w1 = $weekday[$date1 -> format('w')];
    $w2 = $weekday[$date2 -> format('w')];
    // echo $w1;
    $start_date .= $w1;
    $end_date .= $w2;
    // echo '開始日：'. $start_date. '終了日'. $end_date;

    if( isset($_POST['times']) && !isset($_POST['always']) && $_POST['count'] != null ) {
        //最終的な終了日時
        echo $_POST['interval'];
        echo $_POST['period_number'];
        echo $_POST['count'];
        //年月日
        $final_end_date = ( new DateTime($_POST['start']) ) -> modify("+". $_POST['period_number'] * $_POST['count']. $_POST['interval']);
        $date = $final_end_date;  //曜日出力のため一時的に格納
        $final_end_date = $final_end_date -> format('Y年m月d日');

        //曜日
        $weekday = ['(日)', '(月)', '(火)', '(水)', '(木)', '(金)', '(土)'];
        $w = $weekday[$date -> format('w')];
        // echo $w;

        $final_end_date .= $w;
    }

    //表示の修正
    if($_POST['interval'] == 'days') {
        $_POST['interval'] = '日';
    } else if($_POST['interval'] == 'weeks') {
        $_POST['interval'] = '週間';
    } else if($_POST['interval'] == 'months') {
        $_POST['interval'] = 'ヶ月';
    } else if($_POST['interval'] == 'year') {
        $_POST['interval'] = '年';
    }

    //子供の名前の取得
    $sql = "SELECT username FROM children WHERE child_id = '{$_POST['child']}'";
    $stmt = $pdo -> prepare($sql);
    $stmt -> execute();

    $child_name = $stmt -> fetch(PDO::FETCH_ASSOC);

    //成功メッセージ
    $_SESSION['success'] = $child_name['username']. '様 - ID：'. $_POST['child']. ' へのミッション<br>'. 'ミッションID：'. $mission_id. '<br>ミッション名：'. $_POST['name']. '<br>ミッション内容：'. $_POST['detail']. '<br>開始日時：'. $start_date. '<br>期間：'. $_POST['period_number']. $_POST['interval']. '<br>報酬金額：￥'. number_format($_POST['price']). '<br>回数：'. $count;

    if( !isset($_POST['times']) || isset($_POST['always']) || $_POST['count'] == null ) {
        //回数未指定時 or 永続(0)

        //成功メッセージ
        if( isset($_POST['update_mission']) ) {
            //更新時(GET有り)
            $_SESSION['success'] .= '<br>終了日時：'. $end_date. '<br>上記の通りにミッションを更新しました。';
        } else if( isset($_POST['submit_mission']) ) {
            //追加時(GET無し)
            $_SESSION['success'] .= '<br>終了日時：'. $end_date. '<br>上記のミッションの登録が完了しました。';
        }
    } else {
        //回数指定(永続(0)の場合は除く)

        //成功メッセージ
        //GETパラメータ
        if( isset($_POST['update_mission']) ) {
            //更新時(GET有り)
            $_SESSION['success'] .= '<br>1回目の終了日時：'. $end_date. '<br>このミッションを'. $count. 'こなした時の終了日：'. $final_end_date. '<br>上記の通りにミッションを更新しました。';
        } else if( isset($_POST['submit_mission']) ) {
            $_SESSION['success'] .= '<br>1回目の終了日時：'. $end_date. '<br>このミッションを'. $count. 'こなした時の終了日：'. $final_end_date. '<br>上記のミッションの登録が完了しました。';
        }
    }

    //ユーザー選択の選択肢の初期値
    $_SESSION['child_id'] = $_POST['child'];

    header('Location: mission_post.php');
} catch(PDOException $e) {
    //簡易エラー処理
    echo 'DB接続エラー：'. $e -> getMessage();

    exit;
}
