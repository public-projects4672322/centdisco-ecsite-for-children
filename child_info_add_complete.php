<?php
$dsn = 'mysql:host=localhost;dbname=hew2022_it42107;charset=utf8mb4';
$db_user = 'hew2022_it42107';
$db_pass = '';
try {
  //$pdo = new PDO('mysql:dbname=hew2022_it42107;host=hew2022_it42107;charset=utf8','hew2022_it42107','');
  $pdo = new PDO($dsn, $db_user, $db_pass);
  $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
  $pdo->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);

  $sql = "INSERT INTO `children`(`child_id`, `password`, `username`,`username_kana`,`parent_id`) VALUES (:parent_id, :child_id, :password, :username)";
  $prepare = $pdo->prepare($sql);
  $prepare->bindvalue(':parent_id', $_POST['parent_id']);
  $prepare->bindvalue(':child_id', $_POST['child_id']);
  $prepare->bindvalue(':password', $_POST['password']);
  $prepare->bindvalue(':username', $_POST['child_name']);
  $prepare->bindvalue(':username_kana', $_POST['child_name_kana']);
  $prepare->execute();
} catch (PDOException $e) {
  echo 'DB接続エラー ： ' . $e->getMessage();
}
?>

<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>14.4.子供用アカウント追加登録完了ページ</title>
  <meta name="robots" content="none,noindex,nofollow">
</head>

<body>
  <p>Cent Discoへようこそ!</p>
  <h3>登録が完了しました</h3>

  <p>利用者(お子様)の情報を入力(1ページ目/2ページ中)->確認画面->登録完了</p>

  <p>修正が必要な場合は、保護者用アカウントでログイン後マイページへ</p>

  <main id="aa">
    <button>前へ戻る-保護者様のマイページへ</button>
    <button>次へ-Login</button>
  </main>
  <footer class="footer">
    <p>&copy;Cent Disco</p>
  </footer>
</body>

</html>