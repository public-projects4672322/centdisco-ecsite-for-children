<?php
session_start();
if (!isset($_COOKIE['child_key'])) {
  $_SESSION['message'] = 'ログインしてください。';
header('Location: login.php');
  exit;
}


$cart = '';
$message = '';
$id = $_COOKIE['child_key'];

if (isset($_SESSION['message'])) {
    $message = $_SESSION['message'];
    unset($_SESSION['message']);
}
$dsn = 'mysql:host=localhost;dbname=hew2022_it42107;charset=utf8mb4';
$db_user = 'hew2022_it42107';
$db_pass = '';
$pdo = new PDO($dsn, $db_user, $db_pass);
$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
$pdo->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
$sql = "SELECT * FROM child_moneycount WHERE child_id = :id";
$prepare = $pdo->prepare($sql);
$prepare->bindValue(':id', $id);
$prepare->execute();
$money = $prepare->fetch(PDO::FETCH_ASSOC);

$sql = "SELECT * FROM childbalance WHERE child_id = :id";
$prepare = $pdo->prepare($sql);
$prepare->bindValue(':id', $id);
$prepare->execute();
$balance = $prepare->fetch();
$child_balance = $balance['child_balance'];
$to_display_balance = $child_balance;

if(!$money){
  $child_money = [10000, 5000, 1000, 500, 100, 50, 10, 5, 1];
// おつり何枚
  $how_many = [];
  foreach($child_money as $m){
    $how_many[$m] = floor($child_balance / $m);
    $child_balance %= $m;
    $sql = "INSERT INTO child_moneycount VALUE (:child_id, :one, :five, :ten, :fifty, :one_hundred, :five_hundred, :one_thousand, :five_thousand, :ten_thousand)";
    $prepare = $pdo->prepare($sql);
    $prepare->bindValue(':child_id', $id);
    $prepare->bindValue(':one', $how_many[1]);
    $prepare->bindValue(':five', $how_many[5]);
    $prepare->bindValue(':ten', $how_many[10]);
    $prepare->bindValue(':fifty', $how_many[50]);
    $prepare->bindValue(':one_hundred', $how_many[100]);
    $prepare->bindValue(':five_hundred', $how_many[500]);
    $prepare->bindValue(':one_thousand', $how_many[1000]);
    $prepare->bindValue(':five_thousand', $how_many[5000]);
    $prepare->bindValue(':ten_thousand', $how_many[10000]);
    $prepare->execute();
  }
}else{
  $money_sum = 1*$money['one']+5*$money['five']+10*$money['ten']+50*$money['fifty']+100*$money['one_hundred']+500*$money['five_hundred']+1000*$money['one_thousand']+5000*$money['five_thousand']+10000*$money['ten_thousand'];
}
if($money_sum != $balance['child_balance']){
  $child_money = [10000, 5000, 1000, 500, 100, 50, 10, 5, 1];
// おつり何枚
  $how_many = [];
  foreach($child_money as $m){
    $how_many[$m] = floor($child_balance / $m);
    $child_balance %= $m;
  }
  $sql = "UPDATE child_moneycount SET one=:one, five=:five, ten=:ten, fifty=:fifty, one_hundred=:one_hundred, five_hundred=:five_hundred, one_thousand=:one_thousand, five_thousand=:five_thousand, ten_thousand=:ten_thousand WHERE child_id = :id";
  $prepare = $pdo->prepare($sql);
  $prepare->bindValue(':id', $id);
  $prepare->bindValue(':one', $how_many[1]);
  $prepare->bindValue(':five', $how_many[5]);
  $prepare->bindValue(':ten', $how_many[10]);
  $prepare->bindValue(':fifty', $how_many[50]);
  $prepare->bindValue(':one_hundred', $how_many[100]);
  $prepare->bindValue(':five_hundred', $how_many[500]);
  $prepare->bindValue(':one_thousand', $how_many[1000]);
  $prepare->bindValue(':five_thousand', $how_many[5000]);
  $prepare->bindValue(':ten_thousand', $how_many[10000]);
  $prepare->execute();
}


?><!DOCTYPE html>
<html lang="ja">

<head>
  <meta charset="UTF-8">
  <title>cento disco | 支払画面</title>
  <link rel="stylesheet" href="css/reset.css">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/meyer-reset/2.0/reset.min.css">
  <link rel="stylesheet" href="css/style.css">
</head>

<body>
<header class="header">
    <a href="index.php">
      <img src="images/logo001.png" alt="Cent Disco" class="header_logo">
    </a>
    <nav class="gnav">
      <ul class="menu">
        <li><a href="shop.php">Shop</a></li>
        <li><a href="login.php">MyPage&Login</a></li>
        <li><a href="contact.php">Contact</a></li>
        <li>
          <a href="cart.php">
            <img src="images/cart.png" alt="cart" class="header_cart">
          </a>
        </li>
      </ul>
    </nav>
  </header>
  <style>
    body {
      width: 100%;
      margin: 0 auto;
      max-width: 1420px;
      min-width: 600px;
    }

    .container {
      padding: 1px;
      width: 1050px;
      height: 590px;
      margin: 50px auto;
      background: #f0fff0;
      padding: 0.5em 1em;
      border: double 5px #4ec4d3;
    }

    .drop_area {
      position: relative;
      margin: 8px 0 0 80px;
      width: 420px;
      height: 220px;
      border: 1px solid #3a945b;
      background: #f0fff0;
      padding: 0.5em 1em;
      border: solid 3px #95ccff;
      border-radius: 8px;
    }
    .drop_area-tray{
      position: absolute;
      display: inline-block;
      top: -13px;
      left: 10px;
      padding: 0 9px;
      line-height: 1;
      font-size: 19px;
      background: #f0fff0;
      color: #95ccff;
      font-weight: bold;
    }

    .error{
      margin: 50px 0 0 0;
      width: 550px;
      text-align: right;
    }

    #status {
      width: 180px;
      color: #95ccff;
      margin-left: 250px;
    }

    #sum {
      text-align: center;
      width: 500px;
      font-size: 32px;
      margin: -0px 0 0 450px;
    }
    #balance{
      text-align: center;
      width: 500px;
      font-size: 32px;
      margin: -36px 0 0 450px;
    }

    #amount {
      text-align: center;
      width: 500px;
      font-size: 32px;
      margin: 12px 0 0 450px;
    }

    .payment_form {
      width: 150px;
      height: auto;
      text-align: center;
      margin: -120px 0 0 725px;
    }

    .btn {
      display       : inline-block;
      border-radius : 50%;          /* 角丸       */
      font-size     : 20pt;        /* 文字サイズ */
      text-align    : center;      /* 文字位置   */
      cursor        : pointer;     /* カーソル   */
      padding       : 41px 15px;   /* 余白       */
      background    : #00ffff;     /* 背景色     */
      color         : #0000ff;     /* 文字色     */
      line-height   : 1em;         /* 1行の高さ  */
      transition    : .3s;         /* なめらか変化 */
      box-shadow    : 5px 5px 3px #666666;  /* 影の設定 */
      border        : 2px solid #00ffff;    /* 枠の指定 */
    }

    .btn:hover {
      box-shadow    : none;        /* カーソル時の影消去 */
      color         : #00ffff;     /* 背景色     */
      background    : #0000ff;     /* 文字色     */
    }

    .money_area {
      width: 800px;
      display: flex;
      flex-wrap: wrap;
      align-content: flex-start;
      align-items: center;
      margin: 42px 60px 0 60px;
    }

    .money1 {
      width: 200px;
      height: 100px;
      margin: 0 22px 16px 0;
      background-image: url(images/10000yen_back.png);
      background-repeat: no-repeat;
    }

    .money2 {
      width: 200px;
      height: 100px;
      margin: 0 8px 16px 0;
      background-image: url(images/5000yen_back.png);
      background-repeat: no-repeat;
    }

    .money3 {
      width: 200px;
      height: 100px;
      margin: 0 100px 16px 0;
      background-image: url(images/1000yen_back.png);
      background-repeat: no-repeat;
    }

    .money_coin1 {
      width: 100px;
      height: 100px;
      margin: 0 16px 0 0;
      background-image: url(images/500yen_back.png);
      background-repeat: no-repeat;
    }

    .money_coin2 {
      width: 90px;
      height: 100px;
      margin: 0 16px 0 0;
      background-image: url(images/100yen_back.png);
      background-repeat: no-repeat;
    }

    .money_coin3 {
      width: 90px;
      height: 100px;
      margin: 0 16px 0 0;
      background-image: url(images/50yen_back.png);
      background-repeat: no-repeat;
    }

    .money_coin4 {
      width: 100px;
      height: 100px;
      margin: 0 16px 0 0;
      background-image: url(images/10yen_back.png);
      background-repeat: no-repeat;
    }

    .money_coin5 {
      width: 90px;
      height: 100px;
      margin: 0 16px 0 0;
      background-image: url(images/5yen_back.png);
      background-repeat: no-repeat;
    }

    .money_coin6 {
      width: 90px;
      height: 100px;
      margin: 0 16px 0 0;
      background-image: url(images/1yen_back.png);
      background-repeat: no-repeat;
    }

    .item {
      position: absolute;
    }

    .item:hover {
      cursor: pointer;
    }

    .maisu {
      position: absolute;
      z-index: 1000;
      background: #ec4646;
      border-radius: 50%;
      width: 14px;
      height: 14px;
      color: #fff;
      font-size: 12px;
      text-align: center;
      line-height: 14px;
    }
  </style>

  <div class="container">
    <p class="error"><output style="color:red;"><?php echo $message;?></output></p>
    <div class="drop_area">
      <span class="drop_area-tray">トレー</span>
      <p id="balance">あなたの所持金は<?php echo $to_display_balance;?>円です</p>
      <p id="status"></p>
      <p id="sum"></p>
    </div>

    <div class="money_area">
      <div class="money1">
        <?php for ($i = 0; $i < $money['ten_thousand']; $i++) : ?>
          <img class="item" id="<?php echo $i; ?>" src="images/10000yen.jpg" alt="10000">
        <?php endfor;
        $count = $i; ?>
        <div class="maisu"><?php echo $i; ?></div>
      </div>
      <div class="money2">
        <?php for ($i = 0; $i < $money['five_thousand']; $i++) : ?>
          <img class="item" id="<?php echo $count + $i; ?>" src="images/5000yen.jpg" alt="5000">
        <?php endfor;
        $count += $i; ?>
        <div class="maisu"><?php echo $i; ?></div>
      </div>
      <div class="money3">
        <?php for ($i = 0; $i < $money['one_thousand']; $i++) : ?>
          <img class="item" id="<?php echo $count + $i; ?>" src="images/1000yen.jpg" alt="1000">
        <?php endfor;
        $count += $i; ?>
        <div class="maisu"><?php echo $i; ?></div>
      </div>
      <div class="money_coin1">
        <?php for ($i = 0; $i < $money['five_hundred']; $i++) : ?>
          <img class="item" id="<?php echo $count + $i; ?>" src="images/500yen.png" alt="500">
        <?php endfor;
        $count += $i; ?>
        <div class="maisu"><?php echo $i; ?></div>
      </div>
      <div class="money_coin2">
        <?php for ($i = 0; $i < $money['one_hundred']; $i++) : ?>
          <img class="item" id="<?php echo $count + $i; ?>" src="images/100yen.png" alt="100">
        <?php endfor;
        $count += $i; ?>
        <div class="maisu"><?php echo $i; ?></div>
      </div>
      <div class="money_coin3">
        <?php for ($i = 0; $i < $money['fifty']; $i++) : ?>
          <img class="item" id="<?php echo $count + $i; ?>" src="images/50yen.png" alt="50">
        <?php endfor;
        $count += $i; ?>
        <div class="maisu"><?php echo $i; ?></div>
      </div>
      <div class="money_coin4">
        <?php for ($i = 0; $i < $money['ten']; $i++) : ?>
          <img class="item" id="<?php echo $count + $i; ?>" src="images/10yen.png" alt="10">
        <?php endfor;
        $count += $i; ?>
        <div class="maisu"><?php echo $i; ?></div>
      </div>
      <div class="money_coin5">
        <?php for ($i = 0; $i < $money['five']; $i++) : ?>
          <img class="item" id="<?php echo $count + $i; ?>" src="images/5yen.png" alt="5">
        <?php endfor;
        $count += $i; ?>
        <div class="maisu"><?php echo $i; ?></div>
      </div>
      <div class="money_coin6">
        <?php for ($i = 0; $i < $money['one']; $i++) : ?>
          <img class="item" id="<?php echo $count + $i; ?>" src="images/1yen.png" alt="1">
        <?php endfor;
        $count += $i; ?>
        <div class="maisu"><?php echo $i; ?></div>
      </div>
    </div>

    <script src="js/jquery-3.6.0.min.js"></script>
    <script src="js/jquery-ui.min.js"></script>
    <script src="js/payment.js"></script>
    <script>
      var php = {
        cart: "<?php echo $cart; ?>",
      };
    </script>
</body>

</html>
