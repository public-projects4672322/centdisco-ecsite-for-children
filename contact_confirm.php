<?php
session_start();
if(!isset(
    $_POST['mail'],
    $_POST['mail_confirm'],
    $_POST['genre'],
    $_POST['text']
    )){
        $_SESSION['message'] = 'すべてのフィードを入力してください';
        header('Location: contact.php');
    }else if($_POST['mail'] == "" || $_POST['mail_confirm'] == "" || $_POST["text"] == ""){
        $_SESSION['message'] = 'すべてのフィードを入力してください';
        header('Location: contact.php');
}
if($_POST['mail'] == $_POST['mail_confirm']){

}else{
    $_SESSION['message'] = 'メールアドレスをもう一度入力してください';
    header('Location:contact.php');
}
setcookie('mail', $_POST['mail'], time()+60);
setcookie('genre', $_POST['genre'], time()+60);
setcookie('text', $_POST['text'], time()+60);
?>

<!DOCTYPE html>
<html lang="ja">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>お問い合わせ確認</title>
    <link rel="stylesheet" href="css/reset.css">
    <link rel="stylesheet" href="css/style.css">
    <meta name="robots" content="none,noindex,nofollow">
</head>

<body>
<header class="header">
    <a href="index.php">
        <img src="images/logo001.png" alt="Cent Disco" class="header_logo">
    </a>
    <nav class="gnav">
        <ul class="menu">
            <li><a href="shop.php">Shop</a></li>
            <li><a href="login.php">MyPage&Login</a></li>
            <li><a href="contact.php">Contact</a></li>
            
            <li>
              <a href="cart.php">
                <img src="images/cart.png" alt="cart" class="header_cart">
            </a>
            </li>
        </ul>
    </nav>
</header>

<main class="main-content">
    <h2 class="body__title">Contact-お問い合わせ</h2>

    <dl class="form-content">
        <form class="form-content" method="post" action="contact_confirm.php">
            <dt class="form-content__subtitle">01 メールアドレス</dt>
            <dd class="form-content__input"><p><?php echo $_POST['mail']; ?></p></dd>
            <dt class="form-content__subtitle">02 お問い合わせの種類</dt>
            <dd class="form-content__select">
                <p><?php echo $_POST['genre']; ?></p>
            </dd>
            <dt class="form-content__subtitle">03 お問い合わせ内容</dt>
            <dd class="form-content__textarea">
                <p><?php echo $_POST['text']; ?></p>
            </dd>
            <dd class="form-button"><a href="contact.php">前のページに戻る</a></dd>
            <dd class="form-button"><a href="contact_complete.php">送信する</a></dd>
        </form>
    </dl>
</main>

<footer class="footer">
    <p>&copy;Cent Disco</p>
</footer>

</body>
</html>