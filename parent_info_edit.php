<?php
if (!isset($_COOKIE['parent_key'])) {
  $_SESSION['message'] = '保護者用アカウントでログインしてください。';
  header('Location: login.php');
  exit;
}

$dsn = 'mysql:host=localhost;dbname=hew2022_it42107;charset=utf8mb4';
$db_user = 'hew2022_it42107';
$db_pass = '';
$parent_id = $_COOKIE['parent_key'];
try {
  $pdo = new PDO($dsn, $db_user, $db_pass);
  $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
  $pdo->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);

  $sql = "SELECT * FROM parents WHERE parent_id = :parent_id";
  $qry = $pdo->prepare($sql);
  $qry->bindValue(':parent_id', $_COOKIE['parent_key']);
  $qry->execute();

  $user = $qry->fetch();
  $parent_name = $user['parent_name'];
  $parent_id = $user['parent_id'];
  $postal_code = $user['postal_code'];
  $address = $user['address'];
  $mail = $user['mail'];
  $password = $user['password'];

} catch (PDOException $e) {
  echo $e->getMessage();
  exit;
}

?>
<!DOCTYPE html>
<html lang="ja">

<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="stylesheet" href="css/reset.css">
  <link rel="stylesheet" href="css/style.css">
  <title>Cent Disco | 保護者用アカウント管理</title>
  <meta name="robots" content="none,noindex,nofollow">
</head>

<body class="body">
  <header class="header">
    <a href="index.php">
      <img src="images/logo001.png" alt="Cent Disco" class="header_logo">
    </a>
    <nav class="gnav">
      <ul class="menu">
        <li><a href="shop.php">Shop</a></li>
        <li><a href="login.php">MyPage&Login</a></li>
        <li><a href="contact.php">Contact</a></li>

        <li>
          <a href="cart.php">
            <img src="images/cart.png" alt="cart" class="header_cart">
          </a>
        </li>
      </ul>
    </nav>
  </header>

  <main class="main-content">
    <h1 class="body__title">保護者用アカウント編集</h1>

    <dl class="form-content">
      <dt class="form-content__title" style="height:150px;">Top-編集画面</dt>
      <dd class="form-content__description">
        <p>以下、編集したいところを変更してください。</p>
      </dd>
      <hr>
      <form class="form-content" action="parent_info_confirm.php" method="post">
        <dt class="form-content__subtitle  account_info">01-アカウント名</dt>
        <dd class="form-content__input account_info">
          <input type="text" value="<?php echo $parent_name; ?>" name="parent_name">
        </dd>
        <dt class="form-content__subtitle  account_info">02-パスワード</dt>
        <dd class="form-content__input account_info">
          <input type="password" value="<?php echo $password; ?>" name="password">
        </dd>
        <dt class="form-content__subtitle  account_info">03-郵便番号</dt>
        <dd class="form-content__input account_info">
          &#12306;
          <input type="text" value="<?php echo $postal_code; ?>" maxlength="7" name="postal_code">
        </dd>
        <dt class="form-content__subtitle account_info">04-住所</dt>
        <dd class="form-content__input account_info">
          <input type="text" value="<?php echo $address; ?>" name="address">
        </dd>
        <dt class="form-content__subtitle account_info">05-メールアドレス</dt>
        <dd class="form-content__input account_info">
          <input type="text" value="<?php echo $mail; ?>" name="mail">
        </dd>
        <dd class="form-content__submit"><input type="submit" value="編集する"></dd>
      </form>
      <dd class="link-button"><a href="mypage_parent.php">マイページへ</a></dd>
    </dl>
  </main>

  <footer class="footer">
    <p>&copy;Cent Disco</p>
  </footer>

</body>

</html>