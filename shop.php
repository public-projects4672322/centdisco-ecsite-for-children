<?php
$dsn = 'mysql:host=localhost;dbname=hew2022_it42107;charset=utf8mb4';
$db_user = 'hew2022_it42107';
$db_pass = '';

try{
  $pdo = new PDO($dsn, $db_user, $db_pass);
  $pdo->setAttribute(PDO::ATTR_ERRMODE,PDO::ERRMODE_EXCEPTION);
  $pdo->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);

  $sql = "SELECT * FROM products";
  $qry = $pdo->prepare($sql);
  $qry->execute();
  
} catch (PDOException $e) {
  echo 'DB接続エラー ： ' . $e->getMessage();
}

?>
<!DOCTYPE html>
<html lang="ja">

<head>
  <meta charset="utf-8">
  <title>Cent Disco</title>
  <link rel="stylesheet" href="css/reset.css">
  <link rel="stylesheet" href="css/style_product.css">
  <link rel="stylesheet" href="css/style.css">
  <meta name="robots" content="none,noindex,nofollow">
</head>

<body>
  <header class="header">
    <a href="index.php">
      <img src="images/logo001.png" alt="Cent Disco" class="header_logo">
    </a>
    <nav class="gnav">
      <ul class="menu">
        <li><a href="shop.php">Shop</a></li>
        <li><a href="login.php">MyPage&Login</a></li>
        <li><a href="contact.php">Contact</a></li>
        
        <li>
          <a href="cart.php">
            <img src="images/cart.png" alt="cart" class="header_cart">
          </a>
        </li>
      </ul>
    </nav>
  </header>
  <div class="main-content">
    <h2 class="body__title">商品一覧</h2>

    <?php foreach($qry->fetchAll() as $q): 
    $image = 'images/shop/' . $q['image_path'];
    echo '<a class="product-lineup-a" href="shop_item.php?product_id=' . $q['product_id'] . '">';
    ?>
    <div class="product-lineup">
      <?php echo '<img src="' . $image . '" alt="商品画像">'; ?>
      <div class="product-lineup__description">
        <h2>&#9675;<?php echo $q['product_name']; ?></h2>
        <h3>&yen;<?php echo $q['price']; ?></h3>
        <p><?php echo $q['introduction']; ?></p>
      </div>
    </div>
    </a>
    <hr>
    <?php endforeach; ?>

  <footer class="footer">
    <p>&copy;Cent Disco</p>
  </footer>
</body>

</html>