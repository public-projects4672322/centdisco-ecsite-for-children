<?php
session_start();
$message = "";
if (!isset($_COOKIE['child_key'])) {
  $_SESSION['message'] = 'ログインしてください。';
  header('Location: login.php');
  exit;
}
if (isset($_SESSION['message'])) {
  $message = $_SESSION['message'];
  unset($_SESSION['message']);
}

$dsn = 'mysql:host=localhost;dbname=hew2022_it42107;charset=utf8mb4';
$db_user = 'hew2022_it42107';
$db_pass = '';

try {
  $pdo = new PDO($dsn, $db_user, $db_pass);
  $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
  $pdo->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);

  $sql = "SELECT * FROM icon";
  $qry = $pdo->prepare($sql);
  $qry->execute();
} catch (PDOException $e) {
  echo 'DB接続エラー ： ' . $e->getMessage();
}


?>
<!DOCTYPE html>
<html lang="ja">

<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="stylesheet" href="css/style.css">
  <link rel="stylesheet" href="css/reset.css">
  <title>Cent Disco</title>
  <meta name="robots" content="none,noindex,nofollow">
</head>

<body class="body">
  <header class="header">
    <a href="index.php">
      <img src="images/logo001.png" alt="Cent Disco" class="header_logo">
    </a>
    <nav class="gnav">
      <ul class="menu">
        <li><a href="shop.php">Shop</a></li>
        <li><a href="login.php">MyPage&Login</a></li>
        <li><a href="contact.php">Contact</a></li>
        <li>
          <a href="cart.php">
            <img src="images/cart.png" alt="cart" class="header_cart">
          </a>
        </li>
      </ul>
    </nav>
  </header>

  <main class="main-content">
    <h2 class="body__title">プロフィールの編集</h2>

    <dl class="form-content">
      <output style="color:red; margin-bottom:30px;"><?php echo $message; ?></output>
      <form class="form-content" method="post" action="child_change_check.php">
        <dt class="form-content__subtitle">01 ユーザネーム</dt>
        <dd class="form-content__input"><input type="text" name="username" placeholder="変更しない場合は空白"></dd>
        <dt class="form-content__subtitle">02 アイコン画像</dt>
        <dd class="form-content__radio change-icon">
          <?php
          $i = 0;
          foreach ($qry->fetchAll() as $icon) {
            if ($i == 0) {
              echo '<input type="radio" name="icon" id="' . $icon['icon_id'] . '" value="' . $icon['icon_path'] . '" checked>';
            } else {
              echo '<input type="radio" name="icon" id="' . $icon['icon_id'] . '" value="' . $icon['icon_path'] . '">';
            }
            echo '<label for="' . $icon['icon_id'] . '">';
            echo '<img class="change-icon_img" src="images/icon/' . $icon['icon_path'] . '">';
            echo '</label>';
            $i++;
          }


          ?>
        </dd>
        <input type="hidden" name="child_change" value="true">
        <a href="mypage_child.php">
          <button class="button">マイページへ</button>
        </a>
        <dd class="form-content__submit"><input type="submit" value="編集する"></dd>
      </form>
    </dl>
  </main>

  <footer class="footer">
    <p>&copy;Cent Disco</p>
  </footer>

</body>

</html>